      MODULE LOGICAL_MOD
!
!******************************************************************************
!  Module LOGICAL_MOD contains all of the logical switches used by MARIA. 
!  (xxu, 8/10/10,8/10/10)
!  The code is modified AMF. (ywang, 06/27/15)
!
!  Module Variables:
!  ============================================================================
!  (1 ) LDEBUG_AOD_CALC      : ON/OFF switch for AOD calculation       debug
!  
!******************************************************************************
!
      IMPLICIT NONE

      ! Make everything PUBLIC
      PUBLIC

      !=================================================================
      ! MODULE VARIABLES
      !=================================================================

      LOGICAL :: LPRT
      LOGICAL :: LDEBUG_AMF_CALC

      ! Diagonstic
      LOGICAL :: LDIAG_CURR
      LOGICAL :: LDIAG_AVE

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !================================================================= 
      CONTAINS

      SUBROUTINE INIT_LOGICAL

      ! Subourtine INIT_LOGICAL initialize all the logical routine all the 
      ! begining of the simulation

      LPRT             = .FALSE.
      LDEBUG_AMF_CALC  = .FALSE.

      LDIAG_CURR       = .FALSE.
      LDIAG_AVE        = .FALSE. 

      ! Return to the calling routine
      END SUBROUTINE INIT_LOGICAL

!------------------------------------------------------------------------------

      ! End of module
      END MODULE LOGICAL_MOD
