      MODULE INPUT_MOD
!
!******************************************************************************
!  Module INPUT_MOD reads the AMF input file at the start run and passes 
!  the information to serveral other modules, such as 'DIRECTORY_MOD', and
!  'LOGICAL_MOD'. This code is based on GEOS-CHEM-AEROPT code from xxu 
!  (ywang, 06/28/16)
!
!  Module Variables:
!  ============================================================================
!  (1 ) VERBOSE   (LOGICAL ) : Turns on echo-back of lines
!  (2 ) FIRSTCOL  (INTEGER ) : First column of the input file (default=26)
!  (3 ) MAXDIM    (INTEGER ) : Maximum number of substrings to read in 
!  (4 ) FILENAME  (CHAR*255) : MARIA input file name
!  (5 ) TS_DIAG   (INTEGER ) : Placeholder for diagnostic timestep [min]
!  (6 ) TS_DYN    (INTEGER ) : Placeholder for dynamic    timestep [min]
!  
!  Module Routines
!  ============================================================================
!  (1 ) READ_INPUT_FILE   : Driver routine for reading input file "input.aeropt"
!  (2 ) READ_ONE_LINE     : Reads one line at a time
!  (3 ) SPLIT_ONE_LINE    : Splits one line into substrings (by spaces)
!  (4 ) READ_CONTROL_MENU : Reads the control menu
!  (5 ) READ_ND49_MENU    : Reads the ND49 menu
!  (6 ) READ_MODIS_MENU   : Reads the MODIS menu
!  (7 ) READ_DIAG_MENU    : Reads the diagonstic menu
!  (8 ) READ_DEBUG_MENU   : Reads the debug menu
!  (9 ) INIT_INPUT        : Initializes directory & logical variables
!
!******************************************************************************
!
      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "input_mod.f"
      !=================================================================
     
      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these routines
      PUBLIC :: READ_INPUT_FILE

      ! .. variables
      PUBLIC :: TIME_WINDOW

      !=================================================================
      ! MODULE VARIABLES 
      !=================================================================
      LOGICAL            :: VERBOSE  = .FALSE.
      INTEGER, PARAMETER :: FIRSTCOL = 26
      INTEGER, PARAMETER :: MAXDIM   = 255
      CHARACTER(LEN=255) :: FILENAME = 'input.amf'
      CHARACTER(LEN=255) :: TOPTITLE
      INTEGER            :: TS_DIAG
      INTEGER            :: TS_DYN
      INTEGER            :: IU_AMF

      INTEGER            :: TIME_WINDOW ! (units: min)


      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE READ_INPUT_FILE
!
!******************************************************************************
!  Subroutine READ_INPUT_FILE is the driver program for reading the MARIA
!  input file "input.maria" from disk. (xxu, 8/10/10)
!
!  NOTES:
!  ( 1) This original code is modified to AMF input file (ywang,
!  06/28/15)
!******************************************************************************

      ! References to F90 modules
      USE CHARPAK_MOD, ONLY : STRREPL
      USE FILE_MOD,    ONLY : IOERROR
      USE inquireMod,  ONLY : findFreeLUN
      USE TIME_MOD,    ONLY : SYSTEM_TIMESTAMP

      ! Local variables
      LOGICAL            :: EOF
      INTEGER            :: IOS   
      CHARACTER(LEN=1)   :: TAB   = ACHAR(9)
      CHARACTER(LEN=1)   :: SPACE = ' '
      CHARACTER(LEN=255) :: LINE

      !=================================================================
      ! READ_INPUT_FILE begins here!
      !=================================================================

      ! System time stamp
      WRITE( 6, 99  ) SYSTEM_TIMESTAMP()
 99   FORMAT( /, 2x, '===>  START MACHINE TIME: ', a, '  <===', / )

      ! Echo output
      WRITE( 6, '(a  )' ) REPEAT( '=', 79 )
      WRITE( 6, 100     ) TRIM( FILENAME )
 100  FORMAT( 'READ_INPUT_FILE: Reading ', a )

      ! Initialize directory & logical variables
      CALL INIT_INPUT

      ! Find a free file LUN
      IU_AMF = findFreeLUN()

      ! Open file
      OPEN( IU_AMF, FILE=TRIM( FILENAME ), STATUS='OLD', IOSTAT=IOS )
      IF ( IOS /= 0 ) CALL IOERROR( IOS, IU_AMF,'read_input_file:1' )

      ! Read TOPTITLE for binary punch file
      TOPTITLE = READ_ONE_LINE( EOF  )
      IF ( EOF ) RETURN

      ! Loop until EOF
      DO

         ! Read a line from the file, exit if EOF
         LINE = READ_ONE_LINE( EOF )
         IF ( EOF ) EXIT

         ! Replace tab characters in LINE (if any) w/ spaces
         CALL STRREPL( LINE, TAB, SPACE )

         IF      ( INDEX( LINE, 'CONTROL MENU'   ) > 0 ) THEN
            CALL READ_CONTROL_MENU

         ELSEIF  ( INDEX( LINE, 'ND49 MENU'   ) > 0 ) THEN
            CALL READ_ND49_MENU

         ELSEIF  ( INDEX( LINE, 'MODIS MENU'   ) > 0 ) THEN
            CALL READ_MODIS_MENU

         ELSEIF  ( INDEX( LINE, 'DIAGONSTIC MENU'   ) > 0 ) THEN
            CALL READ_DIAG_MENU

         ELSEIF  ( INDEX( LINE, 'DEBUG MENU'     ) > 0 ) THEN
            CALL READ_DEBUG_MENU

         ELSE IF ( INDEX( LINE, 'END OF FILE'      ) > 0 ) THEN
            EXIT

         ENDIF

      ENDDO
      
      ! Close input file
      CLOSE( IU_AMF )

      !=================================================================
      ! Further error-checking and initialization
      !=================================================================

      ! Make sure all directories are valid
      !CALL VALIDATE_DIRECTORIES

      ! Check GEOS-CHEM timesteps
      CALL CHECK_TIME_STEPS

      ! Echo output
      WRITE( 6, '(a)' ) REPEAT( '=', 79 )
           
      ! Return to calling program
      END SUBROUTINE READ_INPUT_FILE

!------------------------------------------------------------------------------

      FUNCTION READ_ONE_LINE( EOF, LOCATION ) RESULT( LINE )
!
!******************************************************************************
!  Subroutine READ_ONE_LINE reads a line from the input file.  If the global 
!  variable VERBOSE is set, the line will be printed to stdout.  READ_ONE_LINE
!  can trap an unexpected EOF if LOCATION is passed.  Otherwise, it will pass
!  a logical flag back to the calling routine, where the error trapping will
!  be done. (bmy, 7/20/04)
! 
!  Arguments as Output:
!  ===========================================================================
!  (1 ) EOF      (CHARACTER) : Logical flag denoting EOF condition
!  (2 ) LOCATION (CHARACTER) : Name of calling routine; traps premature EOF
!
!  Function value:
!  ===========================================================================
!  (1 ) LINE     (CHARACTER) : A line of text as read from the file
!
!  NOTES:
!******************************************************************************
!      
      ! References to F90 modules
      USE FILE_MOD, ONLY : IOERROR

      ! Arguments
      LOGICAL,          INTENT(OUT)          :: EOF
      CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: LOCATION

      ! Local variables
      INTEGER                                :: IOS
      CHARACTER(LEN=255)                     :: LINE, MSG

      !=================================================================
      ! READ_ONE_LINE begins here!
      !=================================================================

      ! Initialize
      EOF = .FALSE.

      ! Read a line from the file
      READ( IU_AMF, '(a)', IOSTAT=IOS ) LINE

      ! IO Status < 0: EOF condition
      IF ( IOS < 0 ) THEN
         EOF = .TRUE.

         ! Trap unexpected EOF -- stop w/ error msg if LOCATION is passed
         ! Otherwise, return EOF to the calling program
         IF ( PRESENT( LOCATION ) ) THEN
            MSG = 'READ_ONE_LINE: error at: ' // TRIM( LOCATION )
            WRITE( 6, '(a)' ) MSG
            WRITE( 6, '(a)' ) 'Unexpected end of file encountered!'
            WRITE( 6, '(a)' ) 'STOP in READ_ONE_LINE (input_mod.f)'
            WRITE( 6, '(a)' ) REPEAT( '=', 79 )
            STOP
         ELSE
            RETURN
         ENDIF
      ENDIF

      ! IO Status > 0: true I/O error condition
      IF ( IOS > 0 ) CALL IOERROR( IOS, IU_AMF, 'read_one_line:1' )

      ! Print the line (if necessary)
      IF ( VERBOSE ) WRITE( 6, '(a)' ) TRIM( LINE )

      ! Return to calling program
      END FUNCTION READ_ONE_LINE

!------------------------------------------------------------------------------

      SUBROUTINE SPLIT_ONE_LINE( SUBSTRS, N_SUBSTRS, N_EXP, LOCATION )
!
!******************************************************************************
!  Subroutine SPLIT_ONE_LINE reads a line from the input file (via routine 
!  READ_ONE_LINE), and separates it into substrings. (bmy, 7/20/04)
!
!  SPLIT_ONE_LINE also checks to see if the number of substrings found is 
!  equal to the number of substrings that we expected to find.  However, if
!  you don't know a-priori how many substrings to expect a-priori, 
!  you can skip the error check.
! 
!  Arguments as Input:
!  ===========================================================================
!  (3 ) N_EXP     (INTEGER  ) : Number of substrings we expect to find
!                               (N_EXP < 0 will skip the error check!)
!  (4 ) LOCATION  (CHARACTER) : Name of routine that called SPLIT_ONE_LINE
!
!  Arguments as Output:
!  ===========================================================================
!  (1 ) SUBSTRS   (CHARACTER) : Array of substrings (separated by " ")
!  (2 ) N_SUBSTRS (INTEGER  ) : Number of substrings actually found
!
!  NOTES:
!******************************************************************************
!
      ! References to F90 modules
      USE CHARPAK_MOD, ONLY: STRSPLIT

      ! Arguments
      CHARACTER(LEN=255), INTENT(OUT) :: SUBSTRS(MAXDIM)
      INTEGER,            INTENT(OUT) :: N_SUBSTRS
      INTEGER,            INTENT(IN)  :: N_EXP
      CHARACTER(LEN=*),   INTENT(IN)  :: LOCATION

      ! Local varaibles
      LOGICAL                         :: EOF
      CHARACTER(LEN=255)              :: LINE, MSG

      !=================================================================
      ! SPLIT_ONE_LINE begins here!
      !=================================================================      

      ! Create error msg
      MSG = 'SPLIT_ONE_LINE: error at ' // TRIM( LOCATION )

      !=================================================================
      ! Read a line from disk
      !=================================================================
      LINE = READ_ONE_LINE( EOF )

      ! STOP on End-of-File w/ error msg
      IF ( EOF ) THEN
         WRITE( 6, '(a)' ) TRIM( MSG )
         WRITE( 6, '(a)' ) 'End of file encountered!'
         WRITE( 6, '(a)' ) 'STOP in SPLIT_ONE_LINE (input_mod.f)!'
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         STOP
      ENDIF

      !=================================================================
      ! Split the lines between spaces -- start at column FIRSTCOL
      !=================================================================
      CALL STRSPLIT( LINE(FIRSTCOL:), ' ', SUBSTRS, N_SUBSTRS )

      ! Sometimes we don't know how many substrings to expect,
      ! if N_EXP is greater than MAXDIM, then skip the error check
      IF ( N_EXP < 0 ) RETURN

      ! Stop if we found the wrong 
      IF ( N_EXP /= N_SUBSTRS ) THEN
         WRITE( 6, '(a)' ) TRIM( MSG )
         WRITE( 6, 100   ) N_EXP, N_SUBSTRS
         WRITE( 6, '(a)' ) 'STOP in SPLIT_ONE_LINE (input_mod.f)!'
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         STOP
 100     FORMAT( 'Expected ',i2, ' substrs but found ',i3 )
      ENDIF

      ! Return to calling program
      END SUBROUTINE SPLIT_ONE_LINE

!------------------------------------------------------------------------------

      SUBROUTINE READ_CONTROL_MENU

      ! References to F90 modules
      USE DIRECTORY_MOD, ONLY : RUN_DIR,          ND49_DIR
      USE DIRECTORY_MOD, ONLY : DIAG_DIR,         MODIS_DIR
      USE DIRECTORY_MOD, ONLY : SCATR_DIR
      USE GRID_MOD,      ONLY : SET_XOFFSET, SET_YOFFSET,  COMPUTE_GRID
      USE TIME_MOD,      ONLY : SET_BEGIN_TIME,   SET_END_TIME
      USE TIME_MOD,      ONLY : SET_CURRENT_TIME

      ! Local variables
      INTEGER            :: N
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)
      INTEGER            :: NYMDb, NHMSb
      INTEGER            :: NYMDe, NHMSe
      INTEGER            :: I0, J0

      !=================================================================
      ! READ_CONTROL_MENU begins here!
      !=================================================================

      ! Start YYYYMMDD, HHMMSS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_control_menu:1' )
      READ( SUBSTRS(1:N), * ) NYMDb, NHMSb

      ! End YYYYMMDD, HHMMSS
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_control_menu:2' )
      READ( SUBSTRS(1:N), * ) NYMDe, NHMSe

      ! Run directory
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:3' )
      READ( SUBSTRS(1:N), '(a)' ) RUN_DIR

      ! ND49   directory 
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:4' )
      READ( SUBSTRS(1:N), '(a)' ) ND49_DIR

      ! MODIS directory
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:5' )
      READ( SUBSTRS(1:N), '(a)' ) MODIS_DIR

      ! Scattering weight directory
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:6' )
      READ( SUBSTRS(1:N), '(a)' ) SCATR_DIR

      ! Output directory
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:7' )
      READ( SUBSTRS(1:N), '(a)' ) DIAG_DIR

      ! I0, J0
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 2, 'read_simulation_menu:8' )
      READ( SUBSTRS(1:N), *     ) I0, J0

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_control_menu:9' )

      !=================================================================
      ! Print to screen
      !=================================================================
      WRITE( 6, '(/,a)') 'CONTROL MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 100     ) 'Start time of run      : ', NYMDb, NHMSb
      WRITE( 6, 100     ) 'End time of run        : ', NYMDe, NHMSe
      WRITE( 6, 110     ) 'Run    directory       : ', TRIM( RUN_DIR   )
      WRITE( 6, 110     ) 'ND49   directory       : ', TRIM( ND49_DIR  )
      WRITE( 6, 110     ) 'MODIS  directory       : ', TRIM( MODIS_DIR )
      WRITE( 6, 110     ) 'SCATR  directory       : ', TRIM( SCATR_DIR )
      WRITE( 6, 110     ) 'Output directory       : ', TRIM( DIAG_DIR  )
      WRITE( 6, 120     ) 'Global offsets I0, J0  : ', I0, J0

      ! Format statements
 100  FORMAT( A, I8.8, 1X, I6.6 )
 110  FORMAT( A, A              )
 120  FORMAT( A, 2I5            )

      !=================================================================
      ! Call setup routines from other modules
      !=================================================================

      ! Set start time of run in "time_mod.f"
      CALL SET_BEGIN_TIME( NYMDb, NHMSb )

      ! Set end time of run in "time_mod.f"
      CALL SET_END_TIME( NYMDe, NHMSe )

      ! Set the current time
      CALL SET_CURRENT_TIME()

      ! Set global offsets
      CALL SET_XOFFSET( I0 )
      CALL SET_YOFFSET( J0 )

      ! Compute lat/lon/surface area variables
      CALL COMPUTE_GRID

      END SUBROUTINE READ_CONTROL_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_ND49_MENU
!
!******************************************************************************
!  Subroutine READ_TRACER_MENU reads the AEROSOL MENU section of the 
!  MARIA input file. (xxu, 8/10/10)
!  This code is modified for AMF. (ywang, 06/29/15)
!******************************************************************************
!
      ! References to F90 modules
      USE FILE_MOD,       ONLY : ND49_FNAME

      ! Local variables
      INTEGER              :: N,  T
      CHARACTER(LEN=255)   :: SUBSTRS(MAXDIM)

      !=================================================================
      ! READ_AEROSOL_MENU begins here!
      !=================================================================

      ! ND49 filename
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_nd49_menu:1' )
      READ( SUBSTRS(1:N), '(a)' ) ND49_FNAME

      ! CTM_FREQ, frequency [min]
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_nd49_menu:2' )
      READ( SUBSTRS(1:N), * ) TS_DIAG

      ! Time window [min]
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_nd49_menu:3' )
      READ( SUBSTRS(1:N), * ) TIME_WINDOW

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_nd49_menu:4' )

      !=================================================================
      ! Print to screen
      !=================================================================
      WRITE( 6, '(/,a)') 'ND49 MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, '(  a)') 'Inst 3-D timeser. file  : ' //
     &                   TRIM(ND49_FNAME)
      WRITE( 6, 110    ) 'Frequency [min]         : ', TS_DIAG
      WRITE( 6, 110    ) 'Time window [min]       : ', TIME_WINDOW
110   FORMAT(  A, I5 )

      END SUBROUTINE READ_ND49_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_MODIS_MENU
!
!******************************************************************************
!  Subroutine READ_MODIS_MENU reads the MODIS MENU section of the 
!  AMF input file. (xxu, 8/10/10)
!******************************************************************************
!
      ! References to F90 modules
      USE FILE_MOD,       ONLY : MODIS_FNAME

      ! Local variables
      INTEGER              :: N,  T
      CHARACTER(LEN=255)   :: SUBSTRS(MAXDIM)

      !=================================================================
      ! READ_AEROSOL_MENU begins here!
      !=================================================================

      ! MODIS filename
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_modis_menu:1' )
      READ( SUBSTRS(1:N), '(a)' ) MODIS_FNAME

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_modis_menu:2' )

      !=================================================================
      ! Print to screen
      !=================================================================
      WRITE( 6, '(/,a)') 'MODIS MENU'
      WRITE( 6, '(  a)') 'MODIS file              : ' //
     &                   TRIM(MODIS_FNAME)
      WRITE( 6, '(  a)') REPEAT( '-', 48 )

      END SUBROUTINE READ_MODIS_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_DIAG_MENU  

      ! References to F90 modules
      USE LOGICAL_MOD,    ONLY : LDIAG_CURR, LDIAG_AVE 

      ! Local variables
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)
      INTEGER            :: N

      !=================================================================
      ! READ_DIAG_MENU begin here
      !=================================================================

      ! LDIAG_CURR_
      CALL SPLIT_ONE_LINE( SUBSTRS, N,  1, 'read_diag_menu:1' )
      READ( SUBSTRS(1:N), * ) LDIAG_CURR

      ! LDIAG_AVE
      CALL SPLIT_ONE_LINE( SUBSTRS, N, -1, 'read_diag_menu:2' )
      READ( SUBSTRS(1:N), * ) LDIAG_AVE

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_diag_menu:3' )

      !=================================================================
      ! Print to screen
      !=================================================================
      WRITE( 6, '(/,a)') 'DIAGONSTIC MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 100 ) 'Turn on current         : ', LDIAG_CURR
      WRITE( 6, 100 ) 'Turn on average         : ', LDIAG_AVE
100   FORMAT( A, L5 )

      ! Return to the calling Routine
      END SUBROUTINE READ_DIAG_MENU

!------------------------------------------------------------------------------

      SUBROUTINE READ_DEBUG_MENU

      ! References to F90 modules
      USE LOGICAL_MOD,    ONLY : LDEBUG_AMF_CALC, LPRT

      ! Local variables
      INTEGER            :: N, ILAYERS, N_MOMENTS
      CHARACTER(LEN=255) :: SUBSTRS(MAXDIM)

      ! LPRT
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:1' )
      READ( SUBSTRS(1:N), * ) LPRT

      ! DEBUG_AOD_CALC
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:2' )
      READ( SUBSTRS(1:N), * ) LDEBUG_AMF_CALC

      ! Separator line
      CALL SPLIT_ONE_LINE( SUBSTRS, N, 1, 'read_debug_menu:3' )

      !=================================================================
      ! Print to screen
      !=================================================================
      WRITE( 6, '(/,a)') 'DEBUG MENU'
      WRITE( 6, '(  a)') REPEAT( '-', 48 )
      WRITE( 6, 100 ) 'Turn on screen print    : ', LPRT
      WRITE( 6, 100 ) 'Turn on AMF calc. debug : ', LDEBUG_AMF_CALC
100   FORMAT( A, L5 )

      END SUBROUTINE READ_DEBUG_MENU

!------------------------------------------------------------------------------
      
      SUBROUTINE CHECK_TIME_STEPS
!     
!******************************************************************************
!  Subroutine CHECK_TIME_STEPS computes the dynamic time step for the
!  model, currently based on the bpch writen-frequency.
!  (xxu, 11/10/11)
!  
!  NOTES:
!  (1 ) 
!******************************************************************************
!     
      ! References to F90 modules
      USE TIME_MOD,    ONLY : SET_TIMESTEPS

      !=================================================================
      ! CHECK_TIME_STEPS begins here!
      !=================================================================

      ! dynamic time step is based on diag time step
      TS_DYN = TS_DIAG

      ! Initialize timesteps in "time_mod.f"
      CALL SET_TIMESTEPS( DYNAMICS=TS_DYN )

      ! Return to MAIN program
      END SUBROUTINE CHECK_TIME_STEPS 

!------------------------------------------------------------------------------

      SUBROUTINE INIT_INPUT

!
!******************************************************************************
!  Subroutine INIT_INPUT initializes all variables from "directory_mod.f", 
!  "file_mod.f" and "logical_mod.f" for safety's sake. (xxu, 11/10/11)
!******************************************************************************
!
      ! References to F90 modules
      USE DIRECTORY_MOD, ONLY : RUN_DIR,          ND49_DIR
      USE DIRECTORY_MOD, ONLY : DIAG_DIR,         MODIS_DIR
      USE FILE_MOD,      ONLY : ND49_FNAME,       MODIS_FNAME
      USE LOGICAL_MOD,   ONLY : INIT_LOGICAL

      !=================================================================
      ! INIT_INPUT begins here!
      !=================================================================

      ! variables from "directory_mod.f"
      RUN_DIR   = ''
      ND49_DIR  = ''
      DIAG_DIR  = ''
      MODIS_DIR = ''
      
      ! variables from "file_mod.f"
      ND49_FNAME  = ''
      MODIS_FNAME = ''

      ! variables from "logical_mod.f"
      CALL INIT_LOGICAL

      ! Return to the calling routine
      END SUBROUTINE INIT_INPUT

!------------------------------------------------------------------------------

      ! End of module INPUT_MOD
      END MODULE INPUT_MOD
