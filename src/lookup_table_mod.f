      MODULE LOOKUP_TABLE_MOD
!
!*****************************************************************************
! Module LOOKUP_TABLE_MOD contains variables and routines to read data
! for calculating scattering weight (ywang, 07/06/15)
!
! Module Variables:
! ============================================================================
! ( 1)
!
! Module Routines:
! ============================================================================
! ( 1) GET_LOOKUP
! ( 2) INIT_LOOKUP
! ( 3) CLEANUP_LOOKUP
!
!*****************************************************************************
!
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these variables
      PUBLIC :: N_SZA,  N_VZA,    N_LEV
      PUBLIC :: SZA,    VZA
      PUBLIC :: O3_C0,  O3_C1,    O3_C2 
      PUBLIC :: Factor
      PUBLIC :: I0,     I1,       I2,   Ir
      PUBLIC :: Sb
      PUBLIC :: dI0,    dI1,      dI2,  dIr
      PUBLIC :: AIR_P,  ALTITUDE, O3_P, TEMP_P
      PUBLIC :: VLIDORT_PRESS

      ! ... and these routines
      PUBLIC :: INIT_LOOKUP
      PUBLIC :: CLEANUP_LOOKUP
      PUBLIC :: GET_LOOKUP

      !=================================================================
      ! Module Parameters
      !=================================================================

      INTEGER, PARAMETER :: N_SZA = 12
      INTEGER, PARAMETER :: N_VZA = 8
      INTEGER, PARAMETER :: N_LEV = 48

      !=================================================================
      ! Module Variables
      !=================================================================
      REAL*4,  ALLOCATABLE :: SZA(:),      VZA(:)
      REAL*4               :: O3_C0,       O3_C1,       O3_C2
      REAL*4               :: Factor
      REAL*4,  ALLOCATABLE :: I0(:,:),     I1(:,:),     I2(:,:)
      REAL*4,  ALLOCATABLE :: Ir(:,:)
      REAL*4               :: Sb
      REAL*4,  ALLOCATABLE :: dI0(:,:,:),  dI1(:,:,:),  dI2(:,:,:)
      REAL*4,  ALLOCATABLE :: dIr(:,:,:)
      REAL*4,  ALLOCATABLE :: AIR_P(:),    ALTITUDE(:), O3_P(:)
      REAL*4,  ALLOCATABLE :: TEMP_P(:)
      REAL*8,  ALLOCATABLE :: VLIDORT_PRESS(:) ! (unit: hPa)

      !=================================================================
      ! Module Routines -- follow below the the "CONTAIN" statement
      !=================================================================
      CONTAINS
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE GET_LOOKUP
!
!*****************************************************************************
! Subroutine GET_LOOKUP read data from "AMFs_lookup_310_320nm.he5"
! (ywang, 07/07/15)
!
!*****************************************************************************
!
      ! References to F90 modules
      USE DIRECTORY_MOD, ONLY : SCATR_DIR
      USE HDF5
      USE LOGICAL_MOD,   ONLY : LDEBUG_AMF_CALC

      ! Parameters
      INTEGER, PARAMETER :: TOMS_OFFSET = 10 ! TOMS ozone (m325)
      INTEGER, PARAMETER :: LLP_OFFSET  =  0 ! Lower level pressure (1.0)
      INTEGER, PARAMETER :: WL_OFFSET   =  3 ! Wavelength (313 nm)

      ! Local variables
      CHARACTER(LEN=255) :: FILENAME
      CHARACTER(LEN=255) :: DSETNAME
      INTEGER            :: DSETRANK
      INTEGER(HSIZE_T)   :: OFFSET_1(1), OFFSET_3(3), OFFSET_5(5)
      INTEGER(HSIZE_T)   :: OFFSET_6(6)
      INTEGER(HSIZE_T)   :: COUNT_1(1),  COUNT_3(3),  COUNT_5(5)
      INTEGER(HSIZE_T)   :: COUNT_6(6)
      INTEGER            :: MEMRANK
      INTEGER(HSIZE_T)   :: DIMSM_1(1),  DIMSM_2(2),  DIMSM_3(3)

      INTEGER            :: I, J, K
      REAL*4             :: TMP(1)
      
      FILENAME = TRIM(SCATR_DIR) // "AMFs_lookup_310_320nm.he5"

      ! Ozone C0
      DSETNAME = "/Cross sections/Ozone C0"
      DSETRANK    = 1
      OFFSET_1(1) = WL_OFFSET
      COUNT_1(1)  = 1
      MEMRANK     = 1
      DIMSM_1(1)  = 1
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_1,
     &                     COUNT_1,  MEMRANK,  DIMSM_1,  TMP      )
      O3_C0 = TMP(1)

      ! Ozone C1
      DSETNAME = "/Cross sections/Ozone C1"
      DSETRANK    = 1
      OFFSET_1(1) = WL_OFFSET
      COUNT_1(1)  = 1
      MEMRANK     = 1
      DIMSM_1(1)  = 1
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_1,
     &                     COUNT_1,  MEMRANK,  DIMSM_1,  TMP      )
      O3_C1 = TMP(1)

      ! Ozone C2
      DSETNAME = "/Cross sections/Ozone C2"
      DSETRANK    = 1
      OFFSET_1(1) = WL_OFFSET
      COUNT_1(1)  = 1
      MEMRANK     = 1
      DIMSM_1(1)  = 1
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_1,
     &                     COUNT_1,  MEMRANK,  DIMSM_1,  TMP      )
      O3_C2 = TMP(1)

      ! SZA
      DSETNAME    = "/Grid/SZA"
      DSETRANK    = 1
      OFFSET_1(1) = 0
      COUNT_1(1)  = N_SZA
      MEMRANK     = 1
      DIMSM_1     = N_SZA
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_1,
     &                     COUNT_1,  MEMRANK,  DIMSM_1,  SZA      )

      ! VZA
      DSETNAME    = "/Grid/VZA"
      DSETRANK    = 1
      OFFSET_1(1) = 0
      COUNT_1(1)  = N_VZA
      MEMRANK     = 1
      DIMSM_1     = N_VZA
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_1,
     &                     COUNT_1,  MEMRANK,  DIMSM_1,  VZA      )

      !-------------------------
      ! I0, I1, I2, Ir
      !-------------------------
      DSETRANK    = 5
      OFFSET_5    = 0
      OFFSET_5(1) = TOMS_OFFSET
      OFFSET_5(2) = LLP_OFFSET
      OFFSET_5(5) = WL_OFFSET
      COUNT_5     = 1
      COUNT_5(3)  = N_SZA
      COUNT_5(4)  = N_VZA
      MEMRANK     = 2
      DIMSM_2(1)  = N_SZA
      DIMSM_2(2)  = N_VZA
      ! I0
      DSETNAME = "/Intensity/I0"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_5,
     &                     COUNT_5,  MEMRANK,  DIMSM_2,  I0       )
      ! I1
      DSETNAME = "/Intensity/I1"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_5,
     &                     COUNT_5,  MEMRANK,  DIMSM_2,  I1       )
      ! I2
      DSETNAME = "/Intensity/I2"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_5,
     &                     COUNT_5,  MEMRANK,  DIMSM_2,  I2       )
      ! Ir
      DSETNAME = "/Intensity/Ir"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_5,
     &                     COUNT_5,  MEMRANK,  DIMSM_2,  Ir       )

      ! Sb
      DSETNAME = "/Intensity/Sb"
      DSETRANK    = 3
      OFFSET_3(1) = TOMS_OFFSET
      OFFSET_3(2) = LLP_OFFSET
      OFFSET_3(3) = WL_OFFSET
      COUNT_3     = 1
      MEMRANK     = 1
      DIMSM_1(1)  = 1
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_3,
     &                     COUNT_3,  MEMRANK,  DIMSM_1,  TMP      )
      Sb = TMP(1)

      ! Factor
      DSETNAME = "/Jacobians/Factor"
      DSETRANK    = 1
      OFFSET_1(1) = 0
      COUNT_1(1)  = 1
      MEMRANK     = 1
      DIMSM_1(1)  = 1
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_1,
     &                     COUNT_1,  MEMRANK,  DIMSM_1,  TMP      )
      Factor = TMP(1)

      !-------------------------
      ! dI0, dI1, dI2, dIr
      !-------------------------
      DSETRANK    = 6
      OFFSET_6    = 0
      OFFSET_6(1) = TOMS_OFFSET
      OFFSET_6(2) = LLP_OFFSET
      OFFSET_6(5) = WL_OFFSET
      COUNT_6     = 1
      COUNT_6(3)  = N_SZA
      COUNT_6(4)  = N_VZA
      COUNT_6(6)  = N_LEV
      MEMRANK     = 3
      DIMSM_3(1)  = N_SZA
      DIMSM_3(2)  = N_VZA
      DIMSM_3(3)  = N_LEV
      ! dI0
      DSETNAME = "/Jacobians/dI0"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_6,
     &                     COUNT_6,  MEMRANK,  DIMSM_3,  dI0      )
      ! dI1
      DSETNAME = "/Jacobians/dI1"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_6,
     &                     COUNT_6,  MEMRANK,  DIMSM_3,  dI1      )
      ! dI2
      DSETNAME = "/Jacobians/dI2"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_6,
     &                     COUNT_6,  MEMRANK,  DIMSM_3,  dI2      )
      ! dIr
      DSETNAME = "/Jacobians/dIr"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_6,
     &                     COUNT_6,  MEMRANK,  DIMSM_3,  dIr      )

      !-------------------------
      ! Profiles
      !-------------------------
      DSETRANK    = 3
      OFFSET_3(1) = TOMS_OFFSET
      OFFSET_3(2) = LLP_OFFSET
      OFFSET_3(3) = 0
      COUNT_3     = 1
      COUNT_3(3)  = N_LEV
      MEMRANK     = 1
      DIMSM_1(1)  = N_LEV
      ! Air profile
      DSETNAME = "/Profiles/Air profile"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_3,
     &                     COUNT_3,  MEMRANK,  DIMSM_1,  AIR_P    )
      ! Altitude
      DSETNAME = "/Profiles/Altitude"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_3,
     &                     COUNT_3,  MEMRANK,  DIMSM_1,  ALTITUDE )
      ! Ozone profile
      DSETNAME = "/Profiles/Ozone profile"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_3,
     &                     COUNT_3,  MEMRANK,  DIMSM_1,  O3_P     )
      ! Temperature profile
      DSETNAME = "/Profiles/Temperature profile"
      CALL READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET_3,
     &                     COUNT_3,  MEMRANK,  DIMSM_1,  TEMP_P   ) 


      VLIDORT_PRESS = 
     &       (/   0.00772193,    0.0224936,    0.112247,    0.356718,
     &            0.972622,      2.44691,      5.87901,    13.3036,
     &           23.5377,       33.5917,      47.2958,     66.0496,
     &           84.3372,       99.0540,     116.339,     136.856,
     &          161.245,       189.980,      224.898,     265.986,
     &          313.881,       358.581,      396.921,     434.870,
     &          473.106,       510.561,      548.918,     586.483,
     &          624.478,       662.736,      693.982,     720.011,
     &          744.951,       770.585,      795.940,     818.928,
     &          837.292,       851.804,      867.578,     882.513,
     &          898.746,       914.113,      928.573,     944.351,
     &          960.346,       975.394,      990.632,    1006.06      /)


      IF ( LDEBUG_AMF_CALC ) THEN
!      IF ( .FALSE. ) THEN

         WRITE(6, '(A)') REPEAT( '-', 79 )
         WRITE(6, '(3X,A11,E15.5,X,A8)') 
     &            'Ozone C0 = ', O3_C0, 'cm^2/mol'
         WRITE(6, '(3X,A11,E15.5,X,A12)') 
     &             'Ozone C1 = ', O3_C1, 'cm^2/(mol K)'
         WRITE(6, '(3X,A11,E15.5,X,A14)') 
     &             'Ozone C2 = ', O3_C2, 'cm^2/(mol K^2)'

         !--------------------
         ! I0, I1, I2, Ir
         !--------------------
         CALL PRINT_INTENSITY( I0, "I0" )
         CALL PRINT_INTENSITY( I1, "I1" )
         CALL PRINT_INTENSITY( I2, "I2" )
         CALL PRINT_INTENSITY( Ir, "Ir" )

         ! Sb
         WRITE(6, '(A)') REPEAT( '-', 79 )
         WRITE(6, '(20X,A5,F10.6)') "Sb = ", Sb

         ! Factor
         WRITE(6, '(A)') REPEAT( '-', 79 )
         WRITE(6, '(20X,A9,E10.3)') "Factor = ", Factor

         !---------------------
         ! dI0, dI1, dI2, dIr
         !---------------------
         CALL PRINT_JACOBIANS( dI0, "dI0" )         
         CALL PRINT_JACOBIANS( dI1, "dI1" )         
         CALL PRINT_JACOBIANS( dI2, "dI2" )         
         CALL PRINT_JACOBIANS( dIr, "dIr" )

         !---------------------
         ! Profiles
         !---------------------
         WRITE(6, '(A)') REPEAT( '-', 79 )
         WRITE(6, 100)  
         DO K = 1, N_LEV, 1
            WRITE(6, 110) K, AIR_P(K), ALTITUDE(K), VLIDORT_PRESS(K),
     &                       O3_P(K),  TEMP_P(K)
         END DO

 100  FORMAT(7X, 'Air profile', 3X, 'Altitude', 3X, 'Pressure',
     &3X, 'Ozone profile', 3X, 'Temperature profile')
 110  FORMAT(I5, E13.5, F10.3, F10.3, 3X, E13.5, 6X, F10.2)
      END IF



      ! Return to the calling routine
      END SUBROUTINE GET_LOOKUP
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE PRINT_INTENSITY( INTENSITY, LABEL )

      REAL*4,           INTENT(IN) :: INTENSITY(N_SZA,N_VZA)
      CHARACTER(LEN=2), INTENT(IN) :: LABEL
      INTEGER                      :: I, J

      WRITE(6, '(A)') REPEAT( '-', 79 )
      WRITE(6, '(35X,A2)') LABEL
      WRITE(6, 100) "VZA", (VZA(J), J = 1, N_VZA, 1)
      WRITE(6, '(2X,3A)') "SZA"
      DO I = 1, N_SZA, 1
         WRITE(6, 110) SZA(I), (INTENSITY(I,J), J = 1, N_VZA, 1)
      END DO

 100  FORMAT(2X, A, 8F13.1)
 110  FORMAT(F6.1, 8E13.5)

      END SUBROUTINE PRINT_INTENSITY
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE PRINT_JACOBIANS( JACOBIANS, LABEL )

      REAL*4,           INTENT(IN) :: JACOBIANS(N_SZA, N_VZA, N_LEV)
      CHARACTER(LEN=3), INTENT(IN) :: LABEL
      INTEGER                      :: I, J, K

      WRITE(6, '(A)') REPEAT( '-', 79 )
      DO K = 1, N_LEV, 1
!      DO K = 1, 1, 1
         WRITE(6, '(20X,A13,I3)') TRIM(LABEL)//" at level ", K
         WRITE(6, 100) "VZA", (VZA(J), J = 1, N_VZA, 1)
         WRITE(6, '(2X,3A)') "SZA"
         DO I = 1, N_SZA, 1
            WRITE(6, 110) SZA(I), (JACOBIANS(I,J,K), J = 1, N_VZA, 1)
         END DO
         WRITE(6, *)
      END DO

 100  FORMAT(2X, A, 8F13.1)
 110  FORMAT(F6.1, 8E13.5)

      END SUBROUTINE PRINT_JACOBIANS
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE READ_HYPERSLAB( FILENAME, DSETNAME, DSETRANK, OFFSET,
     &                           COUNT,    MEMRANK,  DIMSM,    OUT_DATA
     &                         )
!
!*****************************************************************************
! Subroutine READ_HYPERSLAB read hyperslab from a HDF5 file.
! (ywang, 07/06/15)
!
! Arguements as Input:
! ============================================================================
! ( 1) FILENAME    CHARACTER(LEN=255) : File name
! ( 2) DSETNAME    CHARACTER(LEN=255) : Dataset name
! ( 3) DSETRANK    INTEGER            : Dataset rank in the file
! ( 4) OFFSET      INTEGER(HSIZE_T)   : Hyperslab offset in the file
! ( 5) COUNT       INTEGER(HSIZE_T)   : Size of the hyperslab in the
!                                       file
! ( 6) MEMRANK     INTEGER            : Dataset rank in memory
! ( 7) DIMSM       INTEGER            : Dataset dimensions in memory
!
! Arguements as Output:
! ===========================================================================
! ( 1) OUT_DATA    REAL*4             : Output buffer
!
!*****************************************************************************
!
      ! References to F90 modules
      USE HDF5

      ! Parameters
      CHARACTER(LEN=255), INTENT(IN ) :: FILENAME
      CHARACTER(LEN=255), INTENT(IN ) :: DSETNAME      
      INTEGER,            INTENT(IN ) :: DSETRANK
      INTEGER(HSIZE_T),   INTENT(IN ) :: OFFSET(DSETRANK)
      INTEGER(HSIZE_T),   INTENT(IN ) :: COUNT(DSETRANK)
      INTEGER,            INTENT(IN ) :: MEMRANK
      INTEGER(HSIZE_T),   INTENT(IN ) :: DIMSM(MEMRANK)

      REAL*4,             INTENT(OUT) :: OUT_DATA(*)

      ! Local variables
      INTEGER            :: IERR
      INTEGER(HID_T)     :: FILE_ID
      INTEGER(HID_T)     :: DSET_ID
      INTEGER(HID_T)     :: DATASPACE
      INTEGER(HID_T)     :: MEMSPACE

      !=================================================================
      ! READ_HYPERSLAB begins here!
      !=================================================================

      PRINT*, " - READ_HYPERSLAB: reading ", TRIM(DSETNAME)

      ! Initialize HDF5 interface
      CALL H5OPEN_F( IERR )

      ! Open HDF5 file
      CALL H5FOPEN_F( FILENAME, H5F_ACC_RDONLY_F, FILE_ID, IERR )

      ! Open dataset
      CALL H5DOPEN_F( FILE_ID, DSETNAME, DSET_ID, IERR )

      ! Get dataspace identifier
      CALL H5DGET_SPACE_F( DSET_ID, DATASPACE, IERR )

      ! Select hyperslab in the dataset
      CALL H5SSELECT_HYPERSLAB_F( DATASPACE, H5S_SELECT_SET_F, OFFSET, 
     &                           COUNT, IERR )

      ! Create memory dataspace
      CALL H5SCREATE_SIMPLE_F( MEMRANK, DIMSM, MEMSPACE, IERR )

      ! Read data form hyperslab in the file
      CALL H5DREAD_F( DSET_ID,  H5T_NATIVE_REAL, OUT_DATA, DIMSM, IERR, 
     &                MEMSPACE, DATASPACE )

      ! Close the dataspace for the dataset
      CALL H5SCLOSE_F( DATASPACE, IERR )

      ! Close the memoryspace
      CALL H5SCLOSE_F( MEMSPACE, IERR )

      ! Close the dataset
      CALL H5DCLOSE_F( DSET_ID, IERR )

      ! Close the file
      CALL H5FCLOSE_F( FILE_ID, IERR )

      ! Close HD5 interface
      CALL H5CLOSE_F( IERR )

      ! Return to the calling routine
      END SUBROUTINE READ_HYPERSLAB
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE INIT_LOOKUP( RESET_IS_INIT )
!
!*****************************************************************************
! Subroutine INIT_LOOKUP allocate arrays in this module.
! (ywang, 07/06/15)
!*****************************************************************************
!
      ! Reference to F90 modules
      USE ERROR_MOD,     ONLY : ALLOC_ERR
      
      ! Arguements
      LOGICAL, OPTIONAL    :: RESET_IS_INIT

      ! Local variables
      INTEGER              :: AS
      LOGICAL, SAVE        :: IS_INIT = .FALSE.

      !=================================================================
      ! INIT_LOOKUP begins here
      !=================================================================

      ! Reset IS_INIT as false when clean up
      IF ( PRESENT( RESET_IS_INIT ) ) THEN
         IS_INIT = .FALSE.
         WRITE(6, '(A)') ' - INIT_LOOKUP: IS_INIT is reset to FALSE'
         RETURN
      END IF

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      ! SZA
      ALLOCATE ( SZA(N_SZA), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'SZA' )

      ! VZA
      ALLOCATE ( VZA(N_VZA), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'VZA' )

      ! I0
      ALLOCATE ( I0(N_SZA,N_VZA), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'I0' )

      ! I1
      ALLOCATE ( I1(N_SZA,N_VZA), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'I1' )

      ! I2
      ALLOCATE ( I2(N_SZA,N_VZA), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'I2' )

      ! Ir
      ALLOCATE ( Ir(N_SZA,N_VZA), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'Ir' )

      ! dI0
      ALLOCATE ( dI0(N_SZA,N_VZA,N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'dI0' )

      ! dI1
      ALLOCATE ( dI1(N_SZA,N_VZA,N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'dI1' )

      ! dI2
      ALLOCATE ( dI2(N_SZA,N_VZA,N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'dI2' )

      ! dIr
      ALLOCATE ( dIr(N_SZA,N_VZA,N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'dIr' )

      ! AIR_P
      ALLOCATE ( AIR_P(N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'AIR_P' )

      ! ALTITUDE
      ALLOCATE ( ALTITUDE(N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALTITUDE' )

      ! O3_P
      ALLOCATE ( O3_P(N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'O3_P' )

      ! TEMP_P
      ALLOCATE ( TEMP_P(N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'TEMP_P' )

      ! VLIDORT_PRESS
      ALLOCATE ( VLIDORT_PRESS(N_LEV), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'VLIDORT_PRESS' )

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      WRITE(6, '(A)') 
     &     ' - INIT_LOOKUP: Initialized the lookup table arrays'

      ! Return to the calling routine
      END SUBROUTINE INIT_LOOKUP
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE CLEANUP_LOOKUP

      ! Local varibales
      LOGICAL         :: RESET_IS_INIT = .TRUE.

      ! Clean up the output arrays of this module
      IF ( ALLOCATED( SZA      ) ) DEALLOCATE( SZA      )
      IF ( ALLOCATED( VZA      ) ) DEALLOCATE( VZA      )
      IF ( ALLOCATED( I0       ) ) DEALLOCATE( I0       )
      IF ( ALLOCATED( I1       ) ) DEALLOCATE( I1       )
      IF ( ALLOCATED( I2       ) ) DEALLOCATE( I2       )
      IF ( ALLOCATED( Ir       ) ) DEALLOCATE( Ir       )
      IF ( ALLOCATED( dI0      ) ) DEALLOCATE( dI0      )
      IF ( ALLOCATED( dI1      ) ) DEALLOCATE( dI1      )
      IF ( ALLOCATED( dI2      ) ) DEALLOCATE( dI2      )
      IF ( ALLOCATED( dIr      ) ) DEALLOCATE( dIr      )
      IF ( ALLOCATED( AIR_P    ) ) DEALLOCATE( AIR_P    )
      IF ( ALLOCATED( ALTITUDE ) ) DEALLOCATE( ALTITUDE )
      IF ( ALLOCATED( O3_P     ) ) DEALLOCATE( O3_P     )
      IF ( ALLOCATED( TEMP_P   ) ) DEALLOCATE( TEMP_P   )
      IF ( ALLOCATED( VLIDORT_PRESS )) DEALLOCATE( VLIDORT_PRESS )

      ! Reset IS_INIT in routine INIT_ND49
      CALL INIT_LOOKUP( RESET_IS_INIT )

      END SUBROUTINE CLEANUP_LOOKUP
!
!-----------------------------------------------------------------------------
!
      ! End of module
      END MODULE LOOKUP_TABLE_MOD 
