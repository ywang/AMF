      PROGRAM AMF
!
!*****************************************************************************
!  Model Name  : AMF
!  Date        : 06/28/15
!  Author      : Yi Wang
!                Department of Earth and Atmospheric Sciences
!                University of Nebraska, Lincoln, NE
!                yi.wang@huskers.unl.edu
!  Description : AMF is a model which has the capacity of calculating
!                following air mass factor
!  Notes       :
!  ==========================================================================
!
!*****************************************************************************
!

      ! References to F90 modules
      USE AMF_MOD,          ONLY : READ_OMI_SO2_OBS, CALC_AMF
      USE AMF_MOD,          ONLY : INIT_AMF,         MAKE_ALL_OMI_SO2
      USE INPUT_MOD,        ONLY : READ_INPUT_FILE
      USE LOOKUP_TABLE_MOD, ONLY : INIT_LOOKUP,      GET_LOOKUP
      USE READ_ND49_MOD,    ONLY : INIT_ND49,        GET_ND49_FIELDS
      USE TIME_MOD,         ONLY : SYSTEM_TIMESTAMP, PRINT_CURRENT_TIME
      USE TIME_MOD,         ONLY : SET_CURRENT_TIME, SET_ELAPSED_MIN
      USE TIME_MOD,         ONLY : GET_NYMDb,        GET_NHMSb
      USE TIME_MOD,         ONLY : GET_NYMD,         GET_NHMS
      USE TIME_MOD,         ONLY : GET_TAU,          GET_TAUb
      USE TIME_MOD,         ONLY : GET_TS_DYN
      USE TIME_MOD,         ONLY : GET_ELAPSED_SEC,  GET_DAY_OF_YEAR
      USE TIME_MOD,         ONLY : GET_MONTH,        GET_YEAR
      USE TIME_MOD,         ONLY : GET_SEASON
      USE TIME_MOD,         ONLY : ITS_A_NEW_DAY
      USE TIME_MOD,         ONLY : ITS_TIME_FOR_EXIT

      IMPLICIT NONE

      ! Local variables
      INTEGER            :: N_DYN_STEPS, N_STEP,      N_DYN
      INTEGER            :: YEAR,        MONTH,       DAY
      INTEGER            :: SEASON,      DAY_OF_YEAR
      INTEGER            :: NYMD,        NHMS
      INTEGER            :: NYMDb,       NHMSb
      INTEGER            :: ELAPSED_SEC, NSECb
      REAL*8             :: TAU,         TAUb

      !=================================================================
      ! AMF starts here!
      !=================================================================

      ! Read input file and call init routines from other modules
      CALL READ_INPUT_FILE

      ! Initialization...
      CALL INIT_ND49
      CALL INIT_LOOKUP
      CALL INIT_AMF

      ! Read scattering weight lookup table
      CALL GET_LOOKUP

      ! Define time variables for use below
      NHMS  = GET_NHMS()
      NHMSb = GET_NHMSb()
      NYMD  = GET_NYMD()
      NYMDb = GET_NYMDb()
      TAU   = GET_TAU()
      TAUb  = GET_TAUb()

      !=================================================================
      !      ***** 6 - H O U R   T I M E S T E P   L O O P  *****
      !=================================================================      

      ! Echo message before first timestep
      WRITE( 6, '(a)' )
      WRITE( 6, '(a)' ) REPEAT( '*', 44 )
      WRITE( 6, '(a)' ) '* B e g i n   T i m e   S t e p p i n g !! *'
      WRITE( 6, '(a)' ) REPEAT( '*', 44 )
      WRITE( 6, '(a)' )

      ! NSTEP is the number of dynamic timesteps w/in a 6-h interval
      N_DYN_STEPS = 360 / GET_TS_DYN()

      ! Start a new 6-h loop
      DO

      ! Compute time parameters at start of 6-h loop
      CALL SET_CURRENT_TIME

      ! NSECb is # of seconds at the start of 6-h loop
      NSECb = GET_ELAPSED_SEC()

      ! Get dynamic timestep in seconds
      N_DYN = 60d0 * GET_TS_DYN()

      !=================================================================
      !     ***** D Y N A M I C   T I M E S T E P   L O O P *****
      !=================================================================
      DO N_STEP = 1, N_DYN_STEPS

         ! Compute & print time quantities at start of dyn step
         CALL SET_CURRENT_TIME
         CALL PRINT_CURRENT_TIME

         ! Set time variables for dynamic loop
         DAY_OF_YEAR = GET_DAY_OF_YEAR()
         ELAPSED_SEC = GET_ELAPSED_SEC()
         MONTH       = GET_MONTH()
         NHMS        = GET_NHMS()
         NYMD        = GET_NYMD()
         TAU         = GET_TAU()
         YEAR        = GET_YEAR()
         SEASON      = GET_SEASON()

         ! Read GEOS-Chem ND49 outputs
         CALL GET_ND49_FIELDS

         !==============================================================
         !              ***** D A I L Y   D A T A *****
         !==============================================================
         IF ( ITS_A_NEW_DAY() ) THEN

            CALL READ_OMI_SO2_OBS( NYMD )

         ENDIF

         ! Calculate air mass factor
         CALL CALC_AMF


         !==============================================================
         !       ***** T E S T   F O R   E N D   O F   R U N *****
         !==============================================================
         IF ( ITS_TIME_FOR_EXIT() ) GOTO 9999



         ! Increment elapsed time
         CALL SET_ELAPSED_MIN

      ENDDO

      ENDDO

      !=================================================================
      !           ***** C L E A N U P   A N D   Q U I T *****
      !=================================================================
 9999 CONTINUE

      CALL MAKE_ALL_OMI_SO2

      ! Deallocate dynamic module arrays
      CALL CLEANUP

      ! System time stamp for completing run
      WRITE( 6, 100 ) SYSTEM_TIMESTAMP()
 100  FORMAT( /, 2x, '===>  COMPLETE MACHINE TIME: ', a, '  <===' )

      ! Screen print to indicate the complete running
      WRITE(6,'(/,4x,A)') REPEAT( '*', 50 )
      WRITE(6,'( 12x,A)') 'E N D    O F    S I M U L A T I O N'
      WRITE(6,'(  4x,A)') REPEAT( '*', 50 )

      ! End of the main program
      END PROGRAM AMF
