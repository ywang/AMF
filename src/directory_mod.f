      MODULE DIRECTORY_MOD
!******************************************************************************
!  Module DIRECTORY_MOD contains the directory path variables and filename 
!  variables used by AEROPT. (xxu, 8/10/10, 8/10/10)
!  This code is modified for AMF. (ywang, 06/29/15)
!
!  Module Variables
!  ============================================================================
!  (1 ) RUN_DIR              : run directory
!  (2 ) ND49_DIR             : ND49 directory
!  (3 ) DIAG_DIR             : diagnostic directory
!  (4 ) MODIS_DIR            : modis directory
!******************************************************************************
!
      IMPLICIT NONE

      !=================================================================
      ! MODULE VARIABLES
      !=================================================================
      CHARACTER(LEN=255) :: RUN_DIR
      CHARACTER(LEN=255) :: ND49_DIR
      CHARACTER(LEN=255) :: DIAG_DIR
      CHARACTER(LEN=255) :: MODIS_DIR
      CHARACTER(LEN=255) :: SCATR_DIR

      ! End of module
      END MODULE DIRECTORY_MOD


