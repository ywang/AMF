      MODULE READ_ND49_MOD
!
!*****************************************************************************
!  Module READ_ND49_MOD contains variables and routines to read data
!  outputed by module DIAG49_MOD (ywang, 06/30/15)
!
!  Module Variable:
!  ===========================================================================
!  ( 1) GC_SO2   (REAL*8) :
!
!  Module Routines:
!  ===========================================================================
!  (1 ) GET_ND49_FIELDS
!  (2 ) INIT_ND49
!  (3 ) CLEANUP_ND49
!
!*****************************************************************************
!
      IMPLICIT NONE

      ! Make everything PRIVATE ...
      PRIVATE

      ! ... except these variables
      PUBLIC :: GC_SO2,  GC_TMPU, GC_PEDGE, GC_PCEN
      PUBLIC :: AIRD_3D, BXHT_3D
      PUBLIC :: NI,      NJ,      NL
      PUBLIC :: IFIRST,  JFIRST,  LFIRST

      ! ... and those routines
      PUBLIC :: GET_ND49_FIELDS
      PUBLIC :: INIT_ND49
      PUBLIC :: CLEANUP_ND49

      !=================================================================
      ! Module Variables
      !=================================================================

      REAL*8, ALLOCATABLE :: GC_SO2(:,:,:)    ! (v/v)
      REAL*8, ALLOCATABLE :: GC_TMPU(:,:,:)   ! temperture (K)
      REAL*8, ALLOCATABLE :: GC_PEDGE(:,:,:)  ! PEDGE-$ (prs @ level edges) (hPa)
      REAL*8, ALLOCATABLE :: GC_PCEN(:,:,:)   ! center pressure (hPa)
      REAL*8, ALLOCATABLE :: AIRD_3D(:,:,:)   ! air density (molec/cm3)
      REAL*8, ALLOCATABLE :: BXHT_3D(:,:,:)   ! box height (m)

      INTEGER             :: NI,     NJ,     NL
      INTEGER             :: IFIRST, JFIRST, LFIRST

      !=================================================================
      ! Module Routines -- follow below the the "CONTAIN" statement
      !=================================================================
      CONTAINS
!
!------------------------------------------------------------------------------
!
      SUBROUTINE GET_ND49_FIELDS
!
!******************************************************************************
!  Subroutine GET_ND49_FIELDS get SO2 from files outputed by ND49.
!  (ywang, 04/11/2014)
!
!  NOTES:
!******************************************************************************
!
      ! References to F90 modules
      USE BPCH2_MOD,     ONLY : READ_BPCH2_NEW
      USE DIRECTORY_MOD, ONLY : ND49_DIR
      USE ERROR_MOD,     ONLY : ALLOC_ERR,   ERROR_STOP
      USE FILE_MOD,      ONLY : ND49_FNAME
      USE LOGICAL_MOD,   ONLY : LPRT
      USE TIME_MOD,      ONLY : GET_NYMD,    GET_NHMS,   GET_TAU
      USE TIME_MOD,      ONLY : EXPAND_DATE, YMD_EXTRACT
      USE TIME_MOD,      ONLY : TIMESTAMP_STRING

#     include "define.h"
#     include "CMN_SIZE" ! Size parameters

      ! Local variables
      INTEGER            :: CTMYMD,       CTMHMS
      INTEGER            :: THISYEAR,     THISMONTH,   THISDAY
      INTEGER            :: THISHOUR,     THISMINUTE,  THISSECOND
      INTEGER            :: N,            AS
      REAL*8             :: XTAU
      CHARACTER(LEN=255) :: BPCH_FILE
      CHARACTER(LEN=255) :: CATEGORY
      INTEGER            :: IDT

      REAL*4             :: TMP1(NI,NJ,NL)

      INTEGER            :: I, J, L

      !=================================================================
      ! GET_ND49_FIELDS begins here!
      !=================================================================

      ! Get date and time variables 
      CTMYMD = GET_NYMD()
      CTMHMS = GET_NHMS()
      XTAU   = GET_TAU()

      BPCH_FILE = TRIM(ND49_DIR) // TRIM(ND49_FNAME)
      CALL EXPAND_DATE( BPCH_FILE, CTMYMD, CTMHMS )

      WRITE(6, '(A)') ' Open the ND49 BPCH file: ' // TRIM(BPCH_FILE)

      CATEGORY = 'IJ-AVG-$'
      IDT      = 26
      CALL READ_BPCH2_NEW( BPCH_FILE, CATEGORY, IDT,   XTAU,
     &                            NI,      NJ,   NL,   TMP1 )
      GC_SO2(:,:,:) = TMP1(:,:,:)

      CATEGORY = 'DAO-3D-$'
      IDT      = 3
      CALL READ_BPCH2_NEW( BPCH_FILE, CATEGORY, IDT,   XTAU,
     &                            NI,      NJ,   NL,   TMP1 )
      GC_TMPU(:,:,:) = TMP1(:,:,:)

      CATEGORY = 'PEDGE-$'
      IDT      = 1
      CALL READ_BPCH2_NEW( BPCH_FILE, CATEGORY, IDT,   XTAU,
     &                            NI,      NJ,   NL,   TMP1 )
      GC_PEDGE(:,:,:) = TMP1(:,:,:)

      CATEGORY = 'TIME-SER'
      IDT      = 22
      CALL READ_BPCH2_NEW( BPCH_FILE, CATEGORY, IDT,   XTAU,
     &                            NI,      NJ,   NL,   TMP1 )
      AIRD_3D(:,:,:) = TMP1(:,:,:)

      CATEGORY = 'BXHGHT-$'
      IDT      = 1
      CALL READ_BPCH2_NEW( BPCH_FILE, CATEGORY, IDT,   XTAU,
     &                            NI,      NJ,   NL,   TMP1 )
      BXHT_3D(:,:,:) = TMP1(:,:,:)

      !---------------------------
      ! calculate center pressure
      !---------------------------
      IF ( LLPAR /= NL ) CALL ERROR_STOP( 'LLPAR /= NL',
     &                                    'read_nd49_mod.f' )

      DO L = 1, NL-1, 1
      DO J = 1, NJ,   1
      DO I = 1, NI,   1
         GC_PCEN(I,J,L) = ( GC_PEDGE(I,J,L) + GC_PEDGE(I,J,L+1) ) / 2.0
      END DO
      END DO
      END DO

      DO J = 1, NJ,   1
      DO I = 1, NI,   1
         GC_PCEN(I,J,NL) = ( GC_PEDGE(I,J,NL) + PTOP ) / 2.0
      END DO
      END DO

      WRITE(6, '(A)') ' - GET_ND49_FIELDS: read ' //  TRIM(BPCH_FILE) // 
     &' successfully'

      ! Return to the calling routine
      END SUBROUTINE GET_ND49_FIELDS
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE INIT_ND49( RESET_IS_INIT )
!
!******************************************************************************
!  Subroutine INIT_ND49 allocate arrays in this module.
!  (ywang,06/30/15)
!******************************************************************************
!
      ! References to F90 modules
      USE BPCH2_MOD,     ONLY : GET_DIM, READ_BPCH2_NEW
      USE DIRECTORY_MOD, ONLY : ND49_DIR
      USE ERROR_MOD,     ONLY : ALLOC_ERR        
      USE FILE_MOD,      ONLY : ND49_FNAME
      USE LOGICAL_MOD,   ONLY : LPRT
      USE TIME_MOD,      ONLY : GET_NYMD,    GET_NHMS,   GET_TAU
      USE TIME_MOD,      ONLY : EXPAND_DATE, YMD_EXTRACT
      USE TIME_MOD,      ONLY : TIMESTAMP_STRING

#     include "define.h"
#     include "CMN_SIZE" ! Size parameters

      ! Arguements
      LOGICAL, OPTIONAL       :: RESET_IS_INIT

      ! Local variables
      INTEGER                 :: AS
      LOGICAL, SAVE           :: IS_INIT = .FALSE.

      ! Local variables
      INTEGER            :: CTMYMD,       CTMHMS
      INTEGER            :: THISYEAR,     THISMONTH,   THISDAY
      INTEGER            :: THISHOUR,     THISMINUTE,  THISSECOND
      REAL*8             :: XTAU
      CHARACTER(LEN=255) :: BPCH_FILE
      CHARACTER(LEN=255) :: CATEGORY = 'IJ-AVG-$'
      INTEGER            :: IDT_SO2 = 26

      !=================================================================
      ! INIT_ND49 begins here
      !=================================================================

      ! Reset IS_INIT as false when clean up
      IF ( PRESENT( RESET_IS_INIT ) ) THEN
         IS_INIT = .FALSE.
         PRINT*, ' - INIT_ND49: IS_INIT is reset to FALSE'
         RETURN
      END IF

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      ! Get date and time variables 
      CTMYMD = GET_NYMD()
      CTMHMS = GET_NHMS()
      XTAU   = GET_TAU()

      BPCH_FILE = TRIM(ND49_DIR) // TRIM(ND49_FNAME)
      CALL EXPAND_DATE( BPCH_FILE, CTMYMD, CTMHMS )

      CALL GET_DIM( BPCH_FILE, CATEGORY, IDT_SO2,
     &              XTAU,      NI,       NJ,
     &              NL,        IFIRST,   JFIRST,
     &              LFIRST )

      IF ( LPRT ) THEN

         PRINT*, ' --- INIT_ND49: NI = ', NI
         PRINT*, ' --- INIT_ND49: NJ = ', NJ
         PRINT*, ' --- INIT_ND49: NL = ', NL
         PRINT*, ' --- INIT_ND49: IFIRST = ', IFIRST
         PRINT*, ' --- INIT_ND49: JFIRST = ', JFIRST
         PRINT*, ' --- INIT_ND49: LFIRST = ', LFIRST

      END IF

      ! GC_SO2
      ALLOCATE ( GC_SO2(NI,NJ,NL), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'GC_SO2' )
!      GC_SO2 = 0d0

      ! GC_TMPU
      ALLOCATE ( GC_TMPU(NI,NJ,NL), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'GC_TMPU' )

      ! GC_PEDGE
      ALLOCATE ( GC_PEDGE(NI,NJ,NL), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'GC_PDEGE' )

      ! GC_PCEN
      ALLOCATE ( GC_PCEN(NI,NJ,NL), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'GC_PCEN' )

      ! AIRD_3D
      ALLOCATE ( AIRD_3D(NI,NJ,NL), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'AIRD_3D' )

      ! BXHT_3D
      ALLOCATE ( BXHT_3D(NI,NJ,NL), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'BXHT_3D' )

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      PRINT*, ' - INIT_ND49: Initialized the ND49 arrays'

      ! Return to the calling routine
      END SUBROUTINE INIT_ND49
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE CLEANUP_ND49

      ! Local varibales
      LOGICAL         :: RESET_IS_INIT = .TRUE.

      ! Clean up the output arrays of this module
      IF ( ALLOCATED( GC_SO2   ) ) DEALLOCATE( GC_SO2   )
      IF ( ALLOCATED( GC_TMPU  ) ) DEALLOCATE( GC_TMPU  )
      IF ( ALLOCATED( GC_PEDGE ) ) DEALLOCATE( GC_PEDGE )
      IF ( ALLOCATED( GC_PCEN  ) ) DEALLOCATE( GC_PCEN  )
      IF ( ALLOCATED( AIRD_3D  ) ) DEALLOCATE( AIRD_3D  )
      IF ( ALLOCATED( BXHT_3D  ) ) DEALLOCATE( BXHT_3D  )

      ! Reset IS_INIT in routine INIT_ND49
      CALL INIT_ND49( RESET_IS_INIT )

      END SUBROUTINE CLEANUP_ND49
!
!-----------------------------------------------------------------------------
!
      ! End of module
      END MODULE READ_ND49_MOD
