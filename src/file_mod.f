! $Id: FILE_MOD.f,v 1.1 2010/08/10 09:22:14 xxu Exp $
      MODULE FILE_MOD
!
!******************************************************************************
!  Module FILE_MOD contains file unit numbers, as well as file I/O routines
!  for GEOS-CHEM.  FILE_MOD keeps all of the I/O unit numbers in a single
!  location for convenient access. (bmy, 7/1/02, 8/4/06)
!
!  Module Variables:
!  ============================================================================
!  (1 ) IU_AEROPT   : Unit # for file "input.aeropt"      
!  (2 ) IU_MLIST    : Unit # for file of MODIS granule list
!  (3 ) IU_MODIS    : Unit # for file of MODIS granule
!  (4 ) IU_RRATIO   : Unit # for file of dynamic surface reflectance ratio
!  (6 ) IU_A6       : Unit # for file of GEOS MET a6 file
!  (7 ) IU_A3       : Unit # for file of GEOS MET a3 file
!  (8 ) IU_I6       : Unit # for file of GEOS MET i6 file
!  (9 ) IU_BPCH     : Unit # for file of GEOS-Chem output
!  (10) IU_OPT      : Unit # for file of Mie calculated optical property
!  (11) IU_RSR      : Unit # for file of MODIS RSR file
!  (12) IU_DSTP     : Unit # for file of dust phase function
!  (13) IU_DEBUG1   : Unit # for file of VLIDORT IOP debug
!  
!  (  ) BPCH_FNAME  : 
!  Module Routines
!  ============================================================================
!  (1 ) IOERROR     : Stops w/ error msg output if I/O errors are detected
!  (2 ) FILE_EX_C   : Tests if a directory or file is valid
!  (3 ) FILE_EX_I   : Tests if a file unit refers to a valid file
!  (4 ) CLOSE_FILES : Closes all files at the end of a GEOS-CHEM run
!
!  GEOS-CHEM modules referenced by file_mod.f
!  ============================================================================
!  (1 ) error_mod.f : Module containing NaN and other error check routines
!
!  NOTES:
!******************************************************************************
!
      IMPLICIT NONE

      !=================================================================
      ! MODULE PRIVATE DECLARATIONS -- keep certain internal variables 
      ! and routines from being seen outside "file_mod.f"
      !=================================================================

      ! PRIVATE routines
      PRIVATE :: FILE_EX_C
      PRIVATE :: FILE_EX_I

      !=================================================================
      ! MODULE VARIABLES
      !=================================================================    
!      INTEGER, PARAMETER :: IU_AEROPT   = 10
!      INTEGER, PARAMETER :: IU_BPCH     = 11
!      INTEGER, PARAMETER :: IU_MIE      = 12
!      !INTEGER, PARAMETER :: IU_A6       = 14
!      !INTEGER, PARAMETER :: IU_A3       = 15
!      !INTEGER, PARAMETER :: IU_I6       = 16
!      INTEGER, PARAMETER :: IU_OPT      = 18 
!      !INTEGER, PARAMETER :: IU_RSR      = 19
!      !INTEGER, PARAMETER :: IU_DSTP     = 20 
!      !INTEGER, PARAMETER :: IU_DEBUG1   = 21
      CHARACTER(LEN=255)  :: ND49_FNAME
      CHARACTER(LEN=255)  :: MODIS_FNAME


      !=================================================================
      ! MODULE INTERFACES -- "bind" two or more routines with different
      ! argument types or # of arguments under one unique name
      !=================================================================
      INTERFACE FILE_EXISTS
         MODULE PROCEDURE FILE_EX_C, FILE_EX_I
      END INTERFACE

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAINS" statement 
      !=================================================================
      CONTAINS

!------------------------------------------------------------------------------

      SUBROUTINE IOERROR( ERROR_NUM, UNIT, LOCATION )
!
!******************************************************************************
!  Subroutine IOERRROR prints out I/O error messages.  The error number, 
!  file unit, location, and a brief description will be printed, and 
!  program execution will be halted. (bmy, 5/28/99, 8/4/06)
!
!  Arguments as input:
!  ===========================================================================
!  (1 ) ERROR_NUM : I/O error number (output from the IOSTAT flag)
!  (2 ) UNIT      : Unit # of the file where the I/O error occurred
!  (3 ) LOCATION  : Name of the routine in which the error occurred
!
!  NOTES:
!  (1 ) Now flush the standard output buffer before stopping.  
!        Also updated comments. (bmy, 2/7/00)
!  (2 ) Changed ROUTINE_NAME to LOCATION.  Now also use C-library routines
!        gerror and strerror() to get the error string corresponding to 
!        ERROR_NUM.  For SGI platform, also print the command string that
!        will call the SGI "explain" command, which will yield additional
!        information about the error.  Updated comments, cosmetic changes.  
!        Now also reference "define.h". (bmy, 3/21/02)
!  (3 ) Moved into "file_mod.f".  Now reference GEOS_CHEM_STOP from module
!        "error_mod.f".  Updated comments, cosmetic changes. (bmy, 10/15/02)
!  (4 ) Renamed cpp switch from DEC_COMPAQ to COMPAQ.  Also added code to 
!        display I/O errors on SUN platform. (bmy, 3/23/03)
!  (5 ) Now call GERROR for IBM and INTEL_FC compilers (bmy, 11/6/03)
!  (6 ) Renamed SGI to SGI_MIPS, LINUX to LINUX_PGI, INTEL_FC to INTEL_IFC, 
!        and added LINUX_EFC. (bmy, 12/2/03)
!  (7 ) Now don't flush the buffer for LINUX_EFC (bmy, 4/23/04)
!  (8 ) Modifications for Linux/IFORT Intel v9 compiler (bmy, 11/2/05)
!  (9 ) Now call IFORT_ERRMSG to get the IFORT error messages (bmy, 11/30/05)
!  (10) Remove support for LINUX_IFC & LINUX_EFC compilers (bmy, 8/4/06)
!******************************************************************************
!  
      ! References to F90 modules
      USE ERROR_MOD, ONLY : ERROR_STOP

      IMPLICIT NONE

#     include "define.h"           ! C-preprocessor switches
    
      ! Arguments
      INTEGER,          INTENT(IN) :: ERROR_NUM, UNIT
      CHARACTER(LEN=*), INTENT(IN) :: LOCATION 

      ! Local variables
      CHARACTER(LEN=10)            :: ERROR_NUMSTR 
      CHARACTER(LEN=255)           :: ERROR_MSG
      CHARACTER(LEN=255)           :: EXPLAIN_CMD

      ! External functions
      CHARACTER(LEN=255), EXTERNAL :: GERROR, IFORT_ERRMSG

      !=================================================================
      ! IOERROR begins here!
      !=================================================================

      ! Fancy output
      WRITE( 6, '(a)' ) REPEAT( '=', 79 )

      ! Write error number, unit, location
      WRITE( 6, 110 ) ERROR_NUM, UNIT, TRIM( LOCATION )
 110  FORMAT( 'PROGRAM I/O ERROR ', i5, ' in file unit ', i5, /, 
     &        'Encountered at routine:location ', a )

#if   defined( SGI_MIPS )
      
      !=================================================================
      ! For SGI: print error msg and construct explain command string
      !=================================================================
      IF ( ERROR_NUM == 2 ) THEN   

         ! Error 2 is "file not found", so handle that separately.
         ! You can't use the explain command w/ error 2.
         WRITE( 6, '(/,a)' ) 'Error: No such file or directory'
         
      ELSE

         ! Call SGI strerror routine to convert ERROR_NUM to ERROR_MSG
         ERROR_MSG = GERROR()

         ! Print error message to std output
         WRITE( 6, 120 ) TRIM( ERROR_MSG )
 120     FORMAT( /, 'Error: ', a )
      
         ! Convert ERROR_NUM to string format
         WRITE( ERROR_NUMSTR, '(i10)' ) ERROR_NUM

         ! Construct argument for SGI explain command
         IF ( ERROR_NUM >= 1000 .and. ERROR_NUM < 4000 ) THEN 
            EXPLAIN_CMD = 'explain cf90-' // 
     &                     TRIM( ADJUSTL( ERROR_NUMSTR ))

         ELSE IF ( ERROR_NUM >= 4000 ) THEN
            EXPLAIN_CMD = 'explain lib-'  // 
     &                    TRIM( ADJUSTL( ERROR_NUMSTR ))
         ENDIF
      
         ! Print command string for the SGI explain command
         WRITE( 6, 130 ) TRIM( EXPLAIN_CMD )
 130     FORMAT( /, 'Type "', a, '" at the Unix prompt for an ',
     &              'explanation of the error.' )
      ENDIF

#elif defined( COMPAQ )

      !=================================================================
      ! For COMPAQ/Alpha: call gerror() to get the I/O error msg
      !=================================================================

      ! GERROR returns ERROR_MSG corresponding to ERROR_NUM 
      ERROR_MSG = GERROR()
 
      ! Print error message to std output
      WRITE( 6, 120 ) TRIM( ERROR_MSG )
 120  FORMAT( /, 'Error: ', a )

#elif defined( LINUX_PGI )

      !=================================================================
      ! For LINUX platform w/ PGI compiler
      ! Call gerror() to get the I/O error msg
      !=================================================================

      ! GERROR returns ERROR_MSG corresponding to ERROR_NUM 
      ERROR_MSG = GERROR()
 
      ! Print error message to std output
      WRITE( 6, 120 ) TRIM( ERROR_MSG )
 120  FORMAT( /, 'Error: ', a )

#elif defined( LINUX_IFORT ) 

      !=================================================================
      ! For LINUX platform w/ IFORT v8/v9 compiler:
      ! Call IFORT_ERRMSG to get the error number and message
      !=================================================================

      ! Get an error msg corresponding to this error number
      ERROR_MSG = IFORT_ERRMSG( ERROR_NUM )

      ! Print error message to std output
      WRITE( 6, 120 ) ERROR_NUM, TRIM( ERROR_MSG )
 120  FORMAT( /, 'Error ', i4, ': ', a )

#elif defined( SPARC ) 

      !=================================================================
      ! For SUN/Sparc platform: call gerror() to get the I/O error msg
      !=================================================================

      ! GERROR returns ERROR_MSG corresponding to ERROR_NUM 
      ERROR_MSG = GERROR()
 
      ! Print error message to std output
      WRITE( 6, 120 ) TRIM( ERROR_MSG )
 120  FORMAT( /, 'Error: ', a )

#elif defined( IBM_AIX ) 

      !=================================================================
      ! For IBM/AIX platform: call gerror() to get the I/O error msg
      !=================================================================

      ! GERROR returns ERROR_MSG corresponding to ERROR_NUM 
      ERROR_MSG = GERROR()
 
      ! Print error message to std output
      WRITE( 6, 120 ) TRIM( ERROR_MSG )
 120  FORMAT( /, 'Error: ', a )

#endif
      
      ! Fancy output
      WRITE( 6, '(a)' ) REPEAT( '=', 79 )

#if   !defined( LINUX_EFC )
      CALL FLUSH( 6 )
#endif

      ! Deallocate arrays and stop safely 
      CALL ERROR_STOP( ERROR_MSG, 'FILE_MOD.f' )
      
      ! End of program
      END SUBROUTINE IOERROR

!------------------------------------------------------------------------------

      FUNCTION FILE_EX_C( FILENAME ) RESULT( IT_EXISTS )
!
!******************************************************************************
!  Function FILE_EX_C returns TRUE if FILENAME exists or FALSE otherwise.  
!  This is handled in a platform-independent way.  The argument is of
!  CHARACTER type. (bmy, 3/23/05, 11/2/05)
!
!  Arguments as Input:
!  ============================================================================
!  (1 ) FILENAME (CHARACTER) : Name of file or directory to test
!
!  NOTES: 
!  (1 ) Updated for LINUX/IFORT Intel v9 compiler (bmy, 11/2/05)
!******************************************************************************
!
#     include "define.h"

      ! Arguments
      CHARACTER(LEN=*), INTENT(IN) :: FILENAME

      ! Function value
      LOGICAL                      :: IT_EXISTS

      !=================================================================
      ! FILE_EX_C begins here!
      !=================================================================

#if   defined( COMPAQ )

      !------------------
      ! COMPAQ compiler
      !------------------

      ! Reference external library function
      INTEGER*4, EXTERNAL :: ACCESS

      ! Test whether directory exists for COMPAQ
      IT_EXISTS = ( ACCESS( TRIM( FILENAME ), ' ' ) == 0 )

#else

      !------------------
      ! Other compilers
      !------------------

      ! Test whether directory exists w/ F90 INQUIRE function
      INQUIRE( FILE=TRIM( FILENAME ), EXIST=IT_EXISTS )

#if   defined( LINUX_IFORT ) 
      
      ! Intel IFORT v9 compiler requires use of the DIRECTORY keyword to 
      ! INQUIRE for checking existence of directories.  (bmy, 11/2/05)
      IF ( .not. IT_EXISTS ) THEN
         INQUIRE( DIRECTORY=TRIM( FILENAME ), EXIST=IT_EXISTS )
      ENDIF

#endif

#endif 

      ! Return to calling program
      END FUNCTION FILE_EX_C

!------------------------------------------------------------------------------

      FUNCTION FILE_EX_I( IUNIT ) RESULT( IT_EXISTS )
!
!******************************************************************************
!  Function FILE_EX_I returns TRUE if FILENAME exists or FALSE otherwise.  
!  This is handled in a platform-independent way.  The argument is of
!  INTEGER type. (bmy, 3/23/05)
!
!  Arguments as Input:
!  ============================================================================
!  (1 ) FILENAME (INTEGER) : Name of file unit to test
!
!  NOTES: 
!******************************************************************************
!
#     include "define.h"

      ! Arguments
      INTEGER, INTENT(IN) :: IUNIT

      ! Function value
      LOGICAL             :: IT_EXISTS

      !=================================================================
      ! FILE_EX_I begins here!
      !=================================================================

      ! Test whether file unit exists w/ F90 INQUIRE function
      INQUIRE( IUNIT, EXIST=IT_EXISTS )

      ! Return to calling program
      END FUNCTION FILE_EX_I

!------------------------------------------------------------------------------

      SUBROUTINE CLOSE_FILES
!
!******************************************************************************
!  Subroutine CLOSE_FILES closes files used by GEOS-CHEM.  This should be
!  called only from the end of the "main.f" program. (bmy, 3/4/98, 10/20/05)
!
!  NOTES:
!  (1 ) Moved into "file_mod.f" (bmy, 6/27/02)
!  (2 ) Also close IU_BC (bmy, 3/27/03)
!  (3 ) Removed IU_INPUT and IU_INPTR, these are obsolete.  Also renamed
!        IU_TS to IU_ND48 (bmy, 7/20/04)
!  (4 ) Also close IU_XT (tmf, bmy, 10/20/05)
!******************************************************************************
!     
      !=================================================================
      ! CLOSE_FILES begins here!
      !=================================================================
!      CLOSE( IU_AMF  )

      ! Return to calling program
      END SUBROUTINE CLOSE_FILES

!------------------------------------------------------------------------------
      
      ! End of module
      END MODULE FILE_MOD
