!  $Id: CLEANUP.f, v1.1 2010/01/12 11:43:51 xxu Exp $ 
      SUBROUTINE CLEANUP
!
!******************************************************************************
!  Subroutine CLEANUP deallocates the memory assigned to dynamic allocatable 
!  arrays 
!******************************************************************************
!
      ! References to F90 modules
      USE AMF_MOD,          ONLY : CLEANUP_AMF
      USE LOOKUP_TABLE_MOD, ONLY : CLEANUP_LOOKUP
      USE READ_ND49_MOD,    ONLY : CLEANUP_ND49 

      IMPLICIT NONE

      !================================================================
      ! CLEANUP begins here!
      !================================================================ 

      !================================================================
      ! Print to screen
      !================================================================
      WRITE( 6, '(/a )' ) REPEAT( '*', 79 )
      WRITE( 6, 100     )
100   FORMAT( '  - CLEANUP: deallocating all retrieval arrays now...' )

      CALL CLEANUP_ND49 
      CALL CLEANUP_LOOKUP
      CALL CLEANUP_AMF


      ! Return to the calling routine 
      END SUBROUTINE CLEANUP

