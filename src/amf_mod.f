      MODULE AMF_MOD
!
!*****************************************************************************
! MODULE AMF_MOD contains subroutine necessary SO2 air mass factor
! through decoupling scatttering weight shape factor. The module is
! intended to compare GEOS-Chem coulmn SO2 with OMI PBL SO2. To
! calculate scattering weighting weight, we use look-up table simulated
! from VLIDORT. m325 in TOMS ozone, 1.0 in Lower level pressure, and 313
! nm in Wavelengh is selected.
!
! (ywang, 06/28/14)
!
! Module Variables:
! ============================================================================
!
! Module Routines:
! ============================================================================
! ( 1) READ_OMI_SO2_OBS : Read OMI SO2 obervations
!
!
! ============================================================================
! NOTES:
!
!*****************************************************************************
!
      USE LOOKUP_TABLE_MOD,      ONLY : N_LEV 
      IMPLICIT NONE

! Header files
#include "define.h"
#include "CMN_SIZE"

      !=================================================================
      ! MODULE AMF_MOD DECLARATIONS -- keep certain internal varibales
      ! and routines form being seen outside "amf_mod.f"
      !=================================================================

      ! Make everything PRIVATE...
      PRIVATE

      ! ... except these routines
      PUBLIC :: CALC_AMF
      PUBLIC :: INIT_AMF
      PUBLIC :: CLEANUP_AMF
      PUBLIC :: READ_OMI_SO2_OBS
      PUBLIC :: MAKE_ALL_OMI_SO2

      !=================================================================
      ! MODULE VARIABLES
      !=================================================================

      ! Variables

      ! Record to store OMI PBL SO2 obervations
      TYPE OMI_SO2_OBS
         REAL    :: LON           ! Latitude (units: degree)
         REAL    :: LAT           ! Longitude (units: dgeree)
         REAL*8  :: TIME          ! Time at start of scan (TAI93) (units: s)
         REAL*8  :: SZA           ! Solar zenith angle (units: degree)
         REAL*8  :: VZA           ! View zenith angle (units: dgeree)
         REAL*8  :: V_OMI_SO2     ! OMI vertical SO2 (units: D.U.)
         REAL*8  :: S_OMI_SO2     ! OMI slant column SO2 (units: D.U.)
         REAL*8  :: AMF           ! New air mass factor
         REAL*8  :: V_SO2         ! OMI vertical SO2 correted by new AMF (units: D.U.)
         REAL*8  :: V_GC_SO2      ! GEOS-Chem vertical column SO2 (units: D.U.)
         REAL*8  :: S_GC_SO2      ! GEOS-Chem slant column SO2 (units: D.U.)
      END TYPE OMI_SO2_OBS

      TYPE(OMI_SO2_OBS), ALLOCATABLE :: OMI_SO2(:)

      LOGICAL,           ALLOCATABLE :: FLAGS(:)

      INTEGER                        :: N_SO2
      INTEGER                        :: N_CURR

      REAL*8,  ALLOCATABLE           :: CURR_GC_V_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: CURR_GC_S_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: CURR_AMF(:,:)
      REAL*8,  ALLOCATABLE           :: CURR_OMI_V_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: CURR_OMI_S_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: CURR_V_SO2(:,:)
      INTEGER, ALLOCATABLE           :: CURR_COUNT(:,:)
      REAL*4,  ALLOCATABLE           :: CURR_DIAG(:,:,:)

      REAL*8,  ALLOCATABLE           :: ALL_GC_V_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: ALL_GC_S_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: ALL_AMF(:,:)
      REAL*8,  ALLOCATABLE           :: ALL_OMI_V_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: ALL_OMI_S_SO2(:,:)
      REAL*8,  ALLOCATABLE           :: ALL_V_SO2(:,:)
      INTEGER, ALLOCATABLE           :: ALL_COUNT(:,:)
      REAL*4,  ALLOCATABLE           :: ALL_DIAG(:,:,:)

      REAL*8                         :: OMI_SZA, OMI_VZA
      INTEGER                        :: SZA_MIN, SZA_MAX
      INTEGER                        :: VZA_MIN, VZA_MAX

      ! for interpolation
      REAL*8                         :: INTER_I0,  INTER_I1
      REAL*8                         :: INTER_I2,  INTER_Ir
      REAL*8                         :: INTER_dI0(N_LEV)
      REAL*8                         :: INTER_dI1(N_LEV)
      REAL*8                         :: INTER_dI2(N_LEV)
      REAL*8                         :: INTER_dIr(N_LEV)

      !=================================================================
      ! MODULE ROUTINES -- follow below the "CONTAIN" statement
      !=================================================================
      CONTAINS
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE READ_OMI_SO2_OBS( YYYYMMDD )
!
!*****************************************************************************
!  Subroutine READ_OMI_SO2_OBS read OMI SO2 data 
!  (ywang, 07/09/15)
!
!  Arguements as Input:
!  ===========================================================================
!  ( 1) YYYYMMDD    (INTEGER) : Current year-month-day
!
!  Module variable as Output:
!  ===========================================================================
!  ( 1) N_SO2       (INTEGER) : Number of OMI SO2 observations for
!  current day
!  ( 2) OMI_SO2 (OMI_SO2_OBS) : OMI SO2 observations
!
!*****************************************************************************
!
      ! Reference to f90 modules
      USE DIRECTORY_MOD,    ONLY : MODIS_DIR
      USE NETCDF
      USE TIME_MOD,         ONLY : EXPAND_DATE, GET_NYMD

      ! Arguements
      INTEGER, INTENT( IN)   :: YYYYMMDD

      ! Local variables
      INTEGER                :: FID,          N_ID
      INTEGER                :: LON_ID,       LAT_ID,     TIME_ID
      INTEGER                :: SZA_ID,       VZA_ID
      INTEGER                :: V_OMI_SO2_ID
      CHARACTER(LEN=255)     :: READ_FILENAME
      CHARACTER(LEN=4)       :: TMP
      INTEGER, ALLOCATABLE   :: TMPINT(:)
      REAL*4,  ALLOCATABLE   :: TMP4(:)
      REAL*8,  ALLOCATABLE   :: TMP8(:)
      LOGICAL                :: LF

      !=================================================================
      ! READ_OMI_SO2_OBS begins here!
      !================================================================= 

      READ_FILENAME = "OMI_L3_SO2_YYYYMMDD.nc"

      ! Expand date tokens in filename
      CALL EXPAND_DATE( READ_FILENAME, YYYYMMDD, 9999 )

      ! Construct complete filename 
      READ_FILENAME = TRIM( MODIS_DIR ) // TRIM( READ_FILENAME )

      ! Does data file exist? If not, it means no data in the day.
      INQUIRE( FILE = TRIM( READ_FILENAME ), EXIST = LF )
      IF ( .NOT. LF ) THEN

         ! No data
         N_SO2 = 0

         WRITE(6, 120) GET_NYMD()

         RETURN

      END IF

 120  FORMAT(' - READ_OMI_SO2_OBS: No data file (warning) in', I10)

      ! Print to screen
      WRITE(6, 100) TRIM( READ_FILENAME )
 100  FORMAT(' - READ_OMI_SO2_OBS: reading file: ', A)

      ! Open file and assign file id (FID)
      CALL CHECK( NF90_OPEN( READ_FILENAME, NF90_NOWRITE, FID ), 0 )

      !-----------------------------------
      ! Get data record IDs
      !-----------------------------------
      CALL CHECK( NF90_INQ_DIMID( FID, "time",    N_ID         ), 100 )

      CALL CHECK( NF90_INQ_VARID( FID, "lon",     LON_ID       ), 101 )
      CALL CHECK( NF90_INQ_VARID( FID, "lat",     LAT_ID       ), 102 )
      CALL CHECK( NF90_INQ_VARID( FID, "time",    TIME_ID      ), 103 )
      CALL CHECK( NF90_INQ_VARID( FID, "SZA",     SZA_ID       ), 104 )
      CALL CHECK( NF90_INQ_VARID( FID, "VZA",     VZA_ID       ), 105 )
      CALL CHECK( NF90_INQ_VARID( FID, "SO2",     V_OMI_SO2_ID ), 106 )

      !------------------------------------
      ! Read dimensions
      !------------------------------------

      ! Read number of observations, N_SO2
      CALL CHECK( NF90_INQUIRE_DIMENSION( FID, N_ID, TMP, N_SO2 ), 200 )

      ! Print to screen
      WRITE(6, 110) N_SO2, GET_NYMD()
 110  FORMAT('      Number of OMI SO2 observations: ' I10, ' in ' I10)

      !-------------------------------------
      ! Read 1D data
      !-------------------------------------

      ! Allocate temporal arrays for 1D data
      ALLOCATE( TMPINT(N_SO2) )
      ALLOCATE( TMP4(N_SO2)   )
      ALLOCATE( TMP8(N_SO2)   )
      TMPINT = 0
      TMP4   = 0E0
      TMP8   = 0D0

      ! Allocate OMI SO2 observations array
      IF ( ALLOCATED( OMI_SO2 ) ) DEALLOCATE( OMI_SO2 )
      ALLOCATE( OMI_SO2(N_SO2) )

      IF ( ALLOCATED( FLAGS) ) DEALLOCATE( FLAGS )
      ALLOCATE( FLAGS(N_SO2) )

      ! Read longitude
      CALL CHECK( NF90_GET_VAR( FID, LON_ID, TMP4 ), 301 )
      OMI_SO2(1:N_SO2)%LON = TMP4(1:N_SO2)

      ! Read latitude
      CALL CHECK( NF90_GET_VAR( FID, LAT_ID, TMP4 ), 302 )
      OMI_SO2(1:N_SO2)%LAT = TMP4(1:N_SO2)

      ! Read time
      CALL CHECK( NF90_GET_VAR( FID, TIME_ID, TMP8 ), 303 )
      OMI_SO2(1:N_SO2)%TIME = TMP8(1:N_SO2)

      ! Read SZA
      CALL CHECK( NF90_GET_VAR( FID, SZA_ID, TMP4 ), 304 )
      OMI_SO2(1:N_SO2)%SZA = TMP4(1:N_SO2)

      ! Read VZA
      CALL CHECK( NF90_GET_VAR( FID, VZA_ID, TMP4 ), 305 )
      OMI_SO2(1:N_SO2)%VZA = TMP4(1:N_SO2)

      ! Read ColumnAmountSO2_PBL
      CALL CHECK( NF90_GET_VAR( FID, V_OMI_SO2_ID, TMP4 ), 306 )
      OMI_SO2(1:N_SO2)%V_OMI_SO2 = TMP4(1:N_SO2)

      ! Close the file
      CALL CHECK( NF90_CLOSE( FID ), 9999 )

      DEALLOCATE( TMPINT )
      DEALLOCATE( TMP4   )
      DEALLOCATE( TMP8   )

      ! Return to the calling routines
      END SUBROUTINE READ_OMI_SO2_OBS
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE CALC_AMF
!
!*****************************************************************************
! Subroutine CALC_AMF calulate air mass factor
! (ywang, 07/13/15)
!
! NOTES:
!*****************************************************************************
!
      ! Reference to f90 modules
      USE ERROR_MOD,         ONLY : ERROR_STOP
      USE GRID_MOD,          ONLY : GET_IJ
      USE GRID_MOD,          ONLY : GET_XOFFSET, GET_YOFFSET
      USE LOGICAL_MOD,       ONLY : LDEBUG_AMF_CALC
      USE LOOKUP_TABLE_MOD,  ONLY : N_LEV
      USE LOOKUP_TABLE_MOD,  ONLY : O3_C0,  O3_C1,    O3_C2
      USE LOOKUP_TABLE_MOD,  ONLY : Factor
      USE LOOKUP_TABLE_MOD,  ONLY : I0,     I1,       I2,   Ir
      USE LOOKUP_TABLE_MOD,  ONLY : Sb
      USE LOOKUP_TABLE_MOD,  ONLY : dI0,    dI1,      dI2,  dIr
      USE LOOKUP_TABLE_MOD,  ONLY : AIR_P,  ALTITUDE, O3_P, TEMP_P
      USE LOOKUP_TABLE_MOD,  ONLY : VLIDORT_PRESS
      USE READ_ND49_MOD,     ONLY : IFIRST, JFIRST
      USE READ_ND49_MOD,     ONLY : GC_SO2, GC_TMPU,  GC_PCEN
      USE READ_ND49_MOD,     ONLY : AIRD_3D, BXHT_3D

      IMPLICIT NONE

      ! Header files
#     include "define.h"
#     include "CMN_SIZE"       ! Size parameters

      ! Parameters
      REAL*8, PARAMETER  :: ALBEDO  = 0.05
      REAL*8, PARAMETER  :: OMI_AMF = 0.36
      REAL*8, PARAMETER  :: MOL2DU  = 2.69D20 ! molecules/m^2

      ! Local variables
      INTEGER            :: NT, I,  J,  L, K
      INTEGER            :: GC_I0, GC_J0
      INTEGER            :: IL, L1, L2
      INTEGER            :: IIJJ(2)

      ! These variables are reversed (from top to bottom)
      REAL*8             :: REV_GC_SO2(LLPAR)         ! v/v
      REAL*8             :: REV_GC_TMPU(LLPAR)        ! K
      REAL*8             :: REV_GC_PCEN(LLPAR)        ! hPa
      REAL*8             :: REV_INTER_GC_SO2(N_LEV)   ! v/v
      REAL*8             :: REV_INTER_GC_TMPU(N_LEV)  ! K
      REAL*8             :: REV_INTER_GC_PCEN(N_LEV)  ! hPa
      REAL*8             :: REV_INTER_GC_T(N_LEV)     ! Celsius degree
      REAL*8             :: REV_INTER_GC_TT(N_LEV)    ! (Celsius degree) ^ 2

      REAL*8             :: RADIANCE          ! radiance
      REAL*8             :: PROFILE_WF(N_LEV) ! profile weighting function
      REAL*8             :: O3_XS(N_LEV)
      REAL*8             :: ABS_XS_O3(N_LEV)  ! ozone absorption cross section
      REAL*8             :: SCATW(N_LEV)      ! scattering weight

      REAL*8             :: MIN_GC_PRESS, MAX_GC_PRESS
      

      LOGICAL            :: FIRST_DEBUG ! for debug

      !=================================================================
      ! CALC_AMF begins here!
      !=================================================================

      WRITE(6, '(A)') ' - CALC_AMF: calculate air mass factor'

      IF ( N_SO2 == 0) THEN

         WRITE(6, '(A)') ' - CALC_AMF: No OMI SO2 observations for
     &current day'

         RETURN

      ENDIF

      FIRST_DEBUG = .TRUE.

      ! GET observations in time window
      CALL GET_OBS

      ! No observations for time window
      IF ( N_CURR == 0 ) THEN

         WRITE(6, '(A)') ' - CALC_AMF: No OMI SO2 obsevations for 
     &current time window'

         RETURN

      END IF

 200  FORMAT(7X, 'Altitude (km)', 3X, 'Pressure (hPa)')
 210  FORMAT(I5, F10.3, 3X, F10.3)

      ! Reset
      CURR_GC_V_SO2  = 0D0
      CURR_GC_S_SO2  = 0D0
      CURR_AMF       = 0D0
      CURR_OMI_V_SO2 = 0D0
      CURR_OMI_S_SO2 = 0D0
      CURR_V_SO2     = 0D0
      CURR_COUNT     = 0

      ! Loop for all observations
      DO NT = 1, N_SO2, 1
      IF ( FLAGS(NT) ) THEN

         IIJJ = GET_IJ( REAL(OMI_SO2(NT)%LON ,4),
     &                  REAL(OMI_SO2(NT)%LAT, 4) )

         GC_I0 = GET_XOFFSET( GLOBAL=.TRUE. )
         GC_J0 = GET_YOFFSET( GLOBAL=.TRUE. )

         ! Array maybe not the whole simulation region.
         I    = IIJJ(1) - (IFIRST - GC_I0) + 1
         J    = IIJJ(2) - (JFIRST - GC_J0) + 1

         !--------------------------------
         ! reversed (from top to bottom)
         !--------------------------------
         DO L = 1, LLPAR, 1

            K = LLPAR - L + 1

            IF ( GC_SO2(I,J,K) >= 0D0 ) THEN

               REV_GC_SO2(L)  = GC_SO2(I,J,K)
               REV_GC_TMPU(L) = GC_TMPU(I,J,K)
               REV_GC_PCEN(L) = GC_PCEN(I,J,K) 

            ELSE

               CALL ERROR_STOP( 'GC_SO2(I,J,K) < 0.0', 'amf_mod.f' )

            END IF

         END DO

         !--------------------------------------------
         ! Interpolate REV_GC_SO2 and REV_GC_TMPU at
         ! REV_GC_TMPU into VLIDORT_PRESS
         !--------------------------------------------
         CALL SPLINE( N_LEV, LLPAR, REV_GC_PCEN,   REV_GC_SO2, 
     &                              VLIDORT_PRESS, REV_INTER_GC_SO2  )
         CALL SPLINE( N_LEV, LLPAR, REV_GC_PCEN,   REV_GC_TMPU,
     &                              VLIDORT_PRESS, REV_INTER_GC_TMPU )

         MIN_GC_PRESS = REV_GC_PCEN(1)
         MAX_GC_PRESS = REV_GC_PCEN(LLPAR)

         L1 = MINLOC( ABS( VLIDORT_PRESS - MIN_GC_PRESS ), 1 )
         L2 = MINLOC( ABS( VLIDORT_PRESS - MAX_GC_PRESS ), 1 )

         DO IL = 1, L1-1, 1
            REV_INTER_GC_SO2(IL)  = REV_GC_SO2(1)
            REV_INTER_GC_TMPU(IL) = REV_GC_TMPU(1)
         END DO

         IF ( VLIDORT_PRESS(L1) < MIN_GC_PRESS ) THEN
            REV_INTER_GC_SO2(L1)  = REV_GC_SO2(1)
            REV_INTER_GC_TMPU(L1) = REV_GC_TMPU(1)
         END IF

         DO IL = L2+1, N_LEV, 1
            REV_INTER_GC_SO2(IL)  = REV_GC_SO2(LLPAR)
            REV_INTER_GC_TMPU(IL) = REV_GC_TMPU(LLPAR)
         END DO
         IF ( VLIDORT_PRESS(L2) > MAX_GC_PRESS ) THEN
            REV_INTER_GC_SO2(L2)  = REV_GC_SO2(LLPAR)
            REV_INTER_GC_TMPU(L2) = REV_GC_TMPU(LLPAR)
         END IF

         DO IL = 1, N_LEV, 1

            IF ( REV_INTER_GC_SO2(IL) < 0D0 ) THEN
               REV_INTER_GC_SO2(IL) = MINVAL( REV_GC_SO2 )
            END IF

            IF ( REV_INTER_GC_TMPU(IL) < 0D0 ) THEN
               CALL ERROR_STOP( 'REV_INTER_GC_TMPU(IL) < 0D0',
     &                          'amf_mod.f' )
            END IF

         END DO

         !---------------------------------
         ! Get SZA an VZA for interpolation
         !---------------------------------
         OMI_SZA = REAL( OMI_SO2(NT)%SZA, 4 )
         OMI_VZA = REAL( OMI_SO2(NT)%VZA, 4 )
         CALL INTERPOLATION


         !---------------------------------
         ! Calculate scattering weight
         !---------------------------------

         ! radiance
         RADIANCE = INTER_I0 + INTER_I1 + INTER_I2 +
     &              ( ALBEDO * INTER_Ir ) / ( 1.0 - ( ALBEDO * Sb ) )

         ! profile weighting function
         PROFILE_WF(:) = ( 
     &                    INTER_dI0(:) + INTER_dI1(:) + INTER_dI2(:) + 
     &                    ( ALBEDO * INTER_dIr(:) ) / 
     &                    (1.0 - ( ALBEDO * Sb )  )
     &                   ) / Factor

         ! ozone absorption cross section
         REV_INTER_GC_T(:)  = REV_INTER_GC_TMPU(:) - 273.15
         REV_INTER_GC_TT(:) = REV_INTER_GC_T(:) ** 2
         O3_XS(:) =   O3_C0                        +
     &              ( O3_C1 * REV_INTER_GC_T(:)  ) +
     &              ( O3_C2 * REV_INTER_GC_TT(:) )          
         ABS_XS_O3(:) =  O3_XS(:) * O3_P(:)

         ! scattering weigth
         SCATW(:) = (-PROFILE_WF(:)) / ( RADIANCE * ABS_XS_O3(:) )

         !--------------------
         ! air mass factor
         !--------------------
         OMI_SO2(NT)%AMF = 
     &                  SUM( REV_INTER_GC_SO2(L1:L2) * SCATW(L1:l2) ) /
     &                  SUM( REV_INTER_GC_SO2(L1:L2) )

         !---------------------
         ! OMI_SO2(NT)
         !---------------------
         OMI_SO2(NT)%S_OMI_SO2 = OMI_SO2(NT)%V_OMI_SO2 * OMI_AMF

         OMI_SO2(NT)%V_SO2     = OMI_SO2(NT)%S_OMI_SO2 / OMI_SO2(NT)%AMF

         OMI_SO2(NT)%V_GC_SO2  = 0D0
         DO L = 1, LLPAR, 1
            !-------------------------------
            ! Units converstion for SO2 [v/v => D.U.]
            !   units of GC_SO2(I,J,L): v/v
            !           AIRD_3D(I,J,L): molec/(cm^2)
            !           BXHT_3D(I,J,L): m
            !                   MOL2DU: 2.69D20 molecules/(m^2)
            !     OMI_SO2(NT)%V_GC_SO2: D.U.
            !-------------------------------
            OMI_SO2(NT)%V_GC_SO2 = OMI_SO2(NT)%V_GC_SO2 +
     &                             GC_SO2(I,J,L)  * 
     &                             AIRD_3D(I,J,L) * 1D6 *
     &                             BXHT_3D(I,J,L) / MOL2DU

         END DO

         OMI_SO2(NT)%S_GC_SO2  = OMI_SO2(NT)%V_GC_SO2 * OMI_SO2(NT)%AMF

         !----------------------
         ! current statistics
         !----------------------
         CURR_GC_V_SO2(I,J)  = CURR_GC_V_SO2(I,J)  + 
     &                         OMI_SO2(NT)%V_GC_SO2
         CURR_GC_S_SO2(I,J)  = CURR_GC_S_SO2(I,J)  + 
     &                         OMI_SO2(NT)%S_GC_SO2
         CURR_AMF(I,J)       = CURR_AMF(I,J)       + 
     &                         OMI_SO2(NT)%AMF
         CURR_OMI_V_SO2(I,J) = CURR_OMI_V_SO2(I,J) +
     &                         OMI_SO2(NT)%V_OMI_SO2
         CURR_OMI_S_SO2(I,J) = CURR_OMI_S_SO2(I,J) +
     &                         OMI_SO2(NT)%S_OMI_SO2
         CURR_V_SO2(I,J)     = CURR_V_SO2(I,J)     +
     &                         OMI_SO2(NT)%V_SO2
         CURR_COUNT(I,J)     = CURR_COUNT(I,J)     + 1

         !----------------------
         ! all statistics
         !----------------------
         ALL_GC_V_SO2(I,J)  = ALL_GC_V_SO2(I,J)  +
     &                         OMI_SO2(NT)%V_GC_SO2
         ALL_GC_S_SO2(I,J)  = ALL_GC_S_SO2(I,J)  +
     &                         OMI_SO2(NT)%S_GC_SO2
         ALL_AMF(I,J)       = ALL_AMF(I,J)       +
     &                         OMI_SO2(NT)%AMF
         ALL_OMI_V_SO2(I,J) = ALL_OMI_V_SO2(I,J) +
     &                         OMI_SO2(NT)%V_OMI_SO2
         ALL_OMI_S_SO2(I,J) = ALL_OMI_S_SO2(I,J) +
     &                         OMI_SO2(NT)%S_OMI_SO2
         ALL_V_SO2(I,J)     = ALL_V_SO2(I,J)     +
     &                         OMI_SO2(NT)%V_SO2
         ALL_COUNT(I,J)     = ALL_COUNT(I,J)     + 1
         


         IF ( LDEBUG_AMF_CALC  .AND. FIRST_DEBUG ) THEN

            WRITE(6, 100) NT, OMI_SO2(NT)%TIME
            WRITE(6, 101) OMI_SO2(NT)%LON, OMI_SO2(NT)%LAT
            WRITE(6, 102) I, J
            WRITE(6, '(A16,F10.5,A9)') 
     &               ' --- Radiance = ', RADIANCE, ' W/(cm^2)'
            WRITE(6, 110) OMI_SZA, OMI_VZA
            WRITE(6, 120) SZA_MIN, SZA_MAX
            WRITE(6, 130) VZA_MIN, VZA_MAX
            WRITE(6, 140) INTER_I0, INTER_I1, INTER_I2, INTER_Ir
            WRITE(6, 150)
            DO IL = 1, N_LEV, 1
            WRITE(6, 160) IL, INTER_dI0(IL), INTER_dI1(IL),
     &                        INTER_dI2(IL), INTER_dIr(IL)
            END DO
            WRITE(6, 103)
            DO L = 1, LLPAR, 1
            WRITE(6, 104) L, REV_GC_PCEN(L),   REV_GC_TMPU(L),
     &                       REV_GC_SO2(L) * 1D9
            END DO
            WRITE(6, 161)
            DO IL = 1, N_LEV, 1
            IF ( IL == L1 ) THEN
               WRITE(6, '(A)') REPEAT(' ', 5) //  REPEAT('-', 199)
            END IF
            WRITE(6, 162) IL, ALTITUDE(IL), VLIDORT_PRESS(IL),
     &                    REV_INTER_GC_TMPU(IL), 
     &                    REV_INTER_GC_SO2(IL) * 1D9,
     &                    O3_XS(IL), O3_P(IL), ABS_XS_O3(IL),
     &                    PROFILE_WF(IL), SCATW(IL)
            IF ( IL == L2 ) THEN
               WRITE(6, '(A)') REPEAT(' ', 5) //  REPEAT('-', 199)
            END IF
            END DO
            WRITE(6, '(A11,F10.5)') ' --- AMF = ', OMI_SO2(NT)%AMF
            WRITE(6, '(A38,F10.5)') ' --- OMI_SO2(NT)%V_OMI_SO2 = ', 
     &                                    OMI_SO2(NT)%V_OMI_SO2
            WRITE(6, '(A38,F10.5)') ' --- OMI_SO2(NT)%S_OMI_SO2 = ', 
     &                                    OMI_SO2(NT)%S_OMI_SO2
            WRITE(6, '(A38,F10.5)') ' ---     OMI_SO2(NT)%V_SO2 = ', 
     &                                        OMI_SO2(NT)%V_SO2
            WRITE(6, '(A38,F10.5)') ' ---  OMI_SO2(NT)%V_GC_SO2 = ', 
     &                                     OMI_SO2(NT)%V_GC_SO2
            WRITE(6, '(A38,F10.5)') ' ---  OMI_SO2(NT)%S_GC_SO2 = ',
     &                                     OMI_SO2(NT)%S_GC_SO2


         END IF
         FIRST_DEBUG = .FALSE.

      
      END IF ! FLAGS(NT)
      END DO ! NT

 100  FORMAT( ' --- NT = ' I10 ' TAU93 = ' F18.2 )
 101  FORMAT( ' --- LON = ' F6.2 ' LAT = ' F6.2 )
 102  FORMAT( ' --- I = ' I6 ' J = ' I6 )
 110  FORMAT( ' --- OMI_SZA = ' F6.2 ', OMI_VZA = ' F6.2 )
 120  FORMAT( ' --- SZA_MIN = ' I6,  ', SZA_MAX = ' I6   )
 130  FORMAT( ' --- VZA_MIN = ' I6,  ', VZA_MAX = ' I6   )
 140  FORMAT( ' --- Interpolation I0, Ir, I2, and Ir: ', 4E13.5, 
     &'W/(cm^2)' )
 150  FORMAT( ' --- Interpolation',2X,'dI0',7X,'dI1',9X,'dI2',6X,
     &'dIr (1E-21 W mol)')
 160  FORMAT( ' ---    ' I4, 4E12.3 )
 103  FORMAT( ' --- Reversed GC:  Pressure (hPa)' 3x 'Temperature (K)' 
     &3x 'SO2 (ppbv)' )
 104  FORMAT( ' --- '  I6, 6X, F10.3, 6X, F10.2, 5x, F15.5 )
 161  FORMAT( ' --- Interpolation Altitude', 3X, 'Pressure (hPa)', 3X,
     &   'Temperature (K)', 3X, 'SO2 (ppbv)', 3X, 'O3_XS (cm^2/mol)', 
     &   3X, 'Ozone profile (mol/cm^2)', 3X, 'ABS_XS_O3 (unitless)',
     &   3x, 'Weighting function (W mol)',
     &   3X, 'Scattering weight (unitless)')
 162  FORMAT( 7X, I5, 4X, F10.3, 5X, F10.3, 6X, F10.2, 4x, F12.5, 3x,
     &        E16.5, 3X, E20.5, 4x, E20.5, 6X, E20.5, 12X, F10.5 )

      CALL MAKE_CURRENT_OMI_SO2




      ! Return to the calling routines
      END SUBROUTINE CALC_AMF
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE GET_OBS
!
!*****************************************************************************
!  Subroutine GET_OBS finds all obsevations in the current time window
!  and simulation area.
!  (ywang, 07/17/14)
!
!  Module variable  as Input:
!  ===========================================================================
!  ( 1) N_SO2     (INTEGER) : Number of observation in current day
!  Arguements as Output:
!
!  Module variable as Output:
!  ===========================================================================
!  ( 1) FLAGS     (LOGICAL) : Whether or not a speicific obsevation is
!  in current time window and simulation area.
!  ( 2) N_CURR    (INTEGER) : Number of obsevations in the current time
!  window and simulation area.
!
!*****************************************************************************
!
      ! Reference to f90 module
      USE GRID_MOD,      ONLY : GET_XEDGE, GET_YEDGE
      USE GRID_MOD,      ONLY : GET_XOFFSET, GET_YOFFSET
      USE INPUT_MOD,     ONLY : TIME_WINDOW
      USE READ_ND49_MOD, ONLY : IFIRST, JFIRST
      USE READ_ND49_MOD, ONLY : NI,     NJ
      USE TIME_MOD,      ONLY : GET_JD,   GET_TAU
      USE TIME_MOD,      ONLY : GET_NYMD, GET_NHMS

      ! Local variables
      REAL*8                 :: HALF_TIME_WINDOW
      REAL*8                 :: WINDOW_BEGIN,    WINDOW_END
      REAL*8                 :: JD85,            JD93,      JD93_85
      REAL*8                 :: CURRENT_TAU
      INTEGER                :: NT
      INTEGER                :: GC_I0, GC_J0

!#if defined( NESTED_CH ) || defined( NESTED_NA ) || defined( NESTED_SD )
      REAL*8, SAVE           :: XEDGE_MIN, XEDGE_MAX
      REAL*8, SAVE           :: YEDGE_MIN, YEDGE_MAX
!#endif

      !=================================================================
      ! GET_OBS begins here!
      !=================================================================

      ! Get the difference between JD93 and JD85
      ! In GEOS-Chem, it is since 1/1/1985, while in OMI, it is since
      ! 1/1/1993
      JD85    = GET_JD( 19850000, 000000 )
      JD93    = GET_JD( 19930000, 000000 )
      JD93_85 = JD93 - JD85
      JD93_85 = JD93_85 * 24D0 ! days => hours

      ! Get current GEOS-Chem TAU
      CURRENT_TAU = GET_TAU()

      ! Change current GEOS-Chem TAU into OMI TAU
      CURRENT_TAU = CURRENT_TAU - JD93_85

      ! Change TAU units ( hours => second )
      CURRENT_TAU = CURRENT_TAU * 3600D0

      ! Get half time window
      HALF_TIME_WINDOW = TIME_WINDOW / 2D0
      HALF_TIME_WINDOW = HALF_TIME_WINDOW * 60D0 ! ( minute => second )

      ! Get current time window
      WINDOW_BEGIN = CURRENT_TAU - HALF_TIME_WINDOW
      WINDOW_END   = CURRENT_TAU + HALF_TIME_WINDOW



      GC_I0 = GET_XOFFSET( GLOBAL=.TRUE. )
      GC_J0 = GET_YOFFSET( GLOBAL=.TRUE. )

!#if defined( NESTED_CH ) || defined( NESTED_NA ) || defined( NESTED_SD )
!         XEDGE_MIN = GET_XEDGE( 1 )
!         XEDGE_MAX = GET_XEDGE( IIPAR+1 )
!         YEDGE_MIN = GET_YEDGE( 1 )
!         YEDGE_MAX = GET_YEDGE( JJPAR+1 )
      XEDGE_MIN = GET_XEDGE( IFIRST - GC_I0      )
      XEDGE_MAX = GET_XEDGE( IFIRST - GC_I0 + NI )
      YEDGE_MIN = GET_YEDGE( JFIRST - GC_J0 )
      YEDGE_MAX = GET_YEDGE( JFIRST - GC_J0 + NJ )
      PRINT*, 'Nested region edge limit'
      PRINT*, 'XEDGE_MIN: ', XEDGE_MIN
      PRINT*, 'XEDGE_MAX: ', XEDGE_MAX
      PRINT*, 'YEDGE_MIN: ', YEDGE_MIN
      PRINT*, 'YEDGE_MAX: ', YEDGE_MAX
!#endif

      N_CURR = 0
      DO NT = 1, N_SO2, 1

         IF (      ( OMI_SO2(NT)%TIME >= WINDOW_BEGIN )
     &       .AND. ( OMI_SO2(NT)%TIME <  WINDOW_END   )
!#if defined( NESTED_CH ) || defined( NESTED_NA ) || defined( NESTED_SD )
     &       .AND. ( OMI_SO2(NT)%LON  >= XEDGE_MIN    )
     &       .AND. ( OMI_SO2(NT)%LON  <= XEDGE_MAX    )
     &       .AND. ( OMI_SO2(NT)%LAT  >= YEDGE_MIN    )
     &       .AND. ( OMI_SO2(NT)%LAT  <= YEDGE_MAX    )
!#endif 
     &                                                  ) THEN


            FLAGS(NT) = .TRUE.

            N_CURR    = N_CURR + 1

         ELSE

            FLAGS(NT) = .FALSE.

         END IF

      END DO

      WRITE(6, 100) N_CURR, GET_NHMS()
 100  FORMAT(' - GET_OBS :Number of OMI SO2 observations: ' I10 ' at ' 
     &I10.6)

      ! Return to calling program
      END SUBROUTINE GET_OBS
!
!----------------------------------------------------------------------------
!
      SUBROUTINE CHECK( STATUS, LOCATION )
!
!*****************************************************************************
!  Subroutine CHECK checks the status of calls to netCDF libraries
!  routines
!  (dkh, 02/15/09) 
!
!  Arguments as Input:
!  ===========================================================================
!  (1 ) STATUS    (INTEGER) : Completion status of netCDF library call    
!  (2 ) LOCATION  (INTEGER) : Location at which netCDF library call was
!  made   
!     
!  NOTES:
!
!*****************************************************************************
!
      ! Reference to f90 modules 
      USE ERROR_MOD,    ONLY  : ERROR_STOP
      USE NETCDF

      ! Arguments
      INTEGER, INTENT(IN)    :: STATUS
      INTEGER, INTENT(IN)    :: LOCATION

      !=================================================================
      ! CHECK begins here!
      !=================================================================

      IF ( STATUS /= NF90_NOERR ) THEN
        WRITE(6,*) TRIM( NF90_STRERROR( STATUS ) )
        WRITE(6,*) 'At location = ', LOCATION
        CALL ERROR_STOP('netCDF error', 'omi_so2_mod')
      ENDIF

      ! Return to calling program
      END SUBROUTINE CHECK
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE INIT_AMF( RESET_IS_INIT )
!
!*****************************************************************************
! Subroutine INIT_AMF allocate DIAGNOSTIC arrays in this module
! (ywang, 07/13/15)
!*****************************************************************************
!
      ! Reference to F90 modules
      USE READ_ND49_MOD,     ONLY : NI,     NJ
      USE ERROR_MOD,         ONLY : ALLOC_ERR

      ! Parameters
      LOGICAL, OPTIONAL       :: RESET_IS_INIT

      ! Local variables
      INTEGER                 :: AS
      LOGICAL, SAVE           :: IS_INIT = .FALSE.

      !=================================================================
      ! INIT_AMF begins here.
      !=================================================================

      ! Reset IS_INIT as false when clean up
      IF ( PRESENT( RESET_IS_INIT ) ) THEN
         IS_INIT = .FALSE.
         WRITE(6, '(A)') ' - INIT_AMF: IS_INIT is reset to FALSE'
         RETURN
      END IF

      ! Return if we have already initialized
      IF ( IS_INIT ) RETURN

      ! CURR_GC_V_SO2
      ALLOCATE( CURR_GC_V_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_GC_V_SO2' )
      ! CURR_GC_S_SO2
      ALLOCATE( CURR_GC_S_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_GC_S_SO2' )
      ! CURR_AMF
      ALLOCATE( CURR_AMF(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_AMF' )
      ! CURR_OMI_V_SO2
      ALLOCATE( CURR_OMI_V_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_OMI_V_SO2' )
      ! CURR_OMI_S_SO2
      ALLOCATE( CURR_OMI_S_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_OMI_S_SO2' )
      ! CURR_V_SO2
      ALLOCATE( CURR_V_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_V_SO2' )
      ! CURR_COUNT
      ALLOCATE( CURR_COUNT(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_COUNT' )
      ! CURR_DIAG
      ALLOCATE( CURR_DIAG(NI,NJ,7), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'CURR_DIAG' )

      ! ALL_GC_V_SO2
      ALLOCATE( ALL_GC_V_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_GC_V_SO2' )
      ALL_GC_V_SO2 = 0D0
      ! ALL_GC_S_SO2
      ALLOCATE( ALL_GC_S_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_GC_S_SO2' )
      ALL_GC_S_SO2 = 0D0
      ! ALL_AMF
      ALLOCATE( ALL_AMF(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_AMF' )
      ALL_AMF = 0D0
      ! ALL_OMI_V_SO2
      ALLOCATE( ALL_OMI_V_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_OMI_V_SO2' )
      ALL_OMI_V_SO2 = 0D0
      ! ALL_OMI_S_SO2
      ALLOCATE( ALL_OMI_S_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_OMI_S_SO2' )
      ALL_OMI_S_SO2 = 0D0
      ! ALL_V_SO2
      ALLOCATE( ALL_V_SO2(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_V_SO2' )
      ALL_V_SO2 = 0D0
      ! ALL_COUNT
      ALLOCATE( ALL_COUNT(NI,NJ), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_COUNT' )
      ALL_COUNT = 0
      ! ALL_DIAG
      ALLOCATE( ALL_DIAG(NI,NJ,7), STAT = AS )
      IF ( AS /= 0 ) CALL ALLOC_ERR( 'ALL_DIAG' )

      ! Reset IS_INIT so we do not allocate arrays again
      IS_INIT = .TRUE.

      WRITE(6, '(A)') ' - INIT_AMF: Initialized the AMF diagnistic 
     &arrays'

      ! Return to the calling routine
      END SUBROUTINE INIT_AMF
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE CLEANUP_AMF

      ! Local varibales
      LOGICAL         :: RESET_IS_INIT = .TRUE.

      ! Clean up the output arrays of this module
      IF ( ALLOCATED( CURR_GC_V_SO2  ) ) DEALLOCATE( CURR_GC_V_SO2  )
      IF ( ALLOCATED( CURR_GC_S_SO2  ) ) DEALLOCATE( CURR_GC_S_SO2  )
      IF ( ALLOCATED( CURR_AMF       ) ) DEALLOCATE( CURR_AMF       )
      IF ( ALLOCATED( CURR_OMI_V_SO2 ) ) DEALLOCATE( CURR_OMI_V_SO2 )
      IF ( ALLOCATED( CURR_OMI_S_SO2 ) ) DEALLOCATE( CURR_OMI_S_SO2 )
      IF ( ALLOCATED( CURR_V_SO2     ) ) DEALLOCATE( CURR_V_SO2     )
      IF ( ALLOCATED( CURR_COUNT     ) ) DEALLOCATE( CURR_COUNT     )
      IF ( ALLOCATED( CURR_DIAG      ) ) DEALLOCATE( CURR_DIAG      )

      IF ( ALLOCATED( ALL_GC_V_SO2  ) ) DEALLOCATE( ALL_GC_V_SO2  )
      IF ( ALLOCATED( ALL_GC_S_SO2  ) ) DEALLOCATE( ALL_GC_S_SO2  )
      IF ( ALLOCATED( ALL_AMF       ) ) DEALLOCATE( ALL_AMF       )
      IF ( ALLOCATED( ALL_OMI_V_SO2 ) ) DEALLOCATE( ALL_OMI_V_SO2 )
      IF ( ALLOCATED( ALL_OMI_S_SO2 ) ) DEALLOCATE( ALL_OMI_S_SO2 )
      IF ( ALLOCATED( ALL_V_SO2     ) ) DEALLOCATE( ALL_V_SO2     )
      IF ( ALLOCATED( ALL_COUNT     ) ) DEALLOCATE( ALL_COUNT     )
      IF ( ALLOCATED( ALL_DIAG      ) ) DEALLOCATE( ALL_DIAG      )

      ! Reset IS_INIT in routine INIT_AMF
      CALL INIT_AMF( RESET_IS_INIT )

      ! Return to the calling routine
      END SUBROUTINE CLEANUP_AMF
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE INTERPOLATION
!
!*****************************************************************************
! Suboutine INTERPOLATION do bilinear interpolation for I0, I1, I2, Ir,
! dI0, dI1, dIr, dIr accoring to SZA and VZA
! (ywang, 07/15/18)
!*****************************************************************************
!
      ! Reference to F90 module
      USE ERROR_MOD,         ONLY  : ERROR_STOP
      USE LOOKUP_TABLE_MOD,  ONLY : SZA,    VZA
      USE LOOKUP_TABLE_MOD,  ONLY : I0,     I1,       I2,   Ir
      USE LOOKUP_TABLE_MOD,  ONLY : dI0,    dI1,      dI2,  dIr

      REAL*4          :: DENOM
      REAL*4          :: C11, C12, C21, C22
      
      CALL DETERMINE_SZA_VZA

      DENOM = ( SZA(SZA_MAX) - SZA(SZA_MIN) ) * 
     &        ( VZA(VZA_MAX) - VZA(VZA_MIN) )

      C11 = ( SZA(SZA_MAX) - OMI_SZA      ) *
     &      ( VZA(VZA_MAX) - OMI_VZA      ) /
     &        DENOM
      C12 = ( SZA(SZA_MAX) - OMI_SZA      ) *
     &      ( OMI_VZA      - VZA(VZA_MIN) ) /
     &        DENOM
      C21 = ( OMI_SZA      - SZA(SZA_MIN) ) *
     &      ( VZA(VZA_MAX) - OMI_VZA      ) /
     &        DENOM
      C22 = ( OMI_SZA      - SZA(SZA_MIN) ) *
     &      ( OMI_VZA      - VZA(VZA_MIN) ) /
     &        DENOM

      ! for debug
      IF ( (C11 < 0.0) .OR. (C12 < 0.0) .OR.
     &     (C21 < 0.0) .OR. (C22 < 0.0)     ) THEN
         CALL ERROR_STOP( 'Bilinear interpolation is wrong', 
     &                    'INTERPOLATION' )
      END IF

      ! I0
      INTER_I0 = I0(SZA_MIN,VZA_MIN) * C11 + I0(SZA_MIN,VZA_MAX) * C12 +
     &           I0(SZA_MAX,VZA_MIN) * C21 + I0(SZA_MAX,VZA_MAX) * C22
      ! I1
      INTER_I1 = I1(SZA_MIN,VZA_MIN) * C11 + I1(SZA_MIN,VZA_MAX) * C12 +
     &           I1(SZA_MAX,VZA_MIN) * C21 + I1(SZA_MAX,VZA_MAX) * C22
      ! I2
      INTER_I2 = I2(SZA_MIN,VZA_MIN) * C11 + I2(SZA_MIN,VZA_MAX) * C12 +
     &           I2(SZA_MAX,VZA_MIN) * C21 + I2(SZA_MAX,VZA_MAX) * C22
      ! Ir
      INTER_Ir = Ir(SZA_MIN,VZA_MIN) * C11 + Ir(SZA_MIN,VZA_MAX) * C12 +
     &           Ir(SZA_MAX,VZA_MIN) * C21 + Ir(SZA_MAX,VZA_MAX) * C22

      ! dI0
      INTER_dI0(:) = dI0(SZA_MIN,VZA_MIN,:) * C11 + 
     &               dI0(SZA_MIN,VZA_MAX,:) * C12 +
     &               dI0(SZA_MAX,VZA_MIN,:) * C21 + 
     &               dI0(SZA_MAX,VZA_MAX,:) * C22
      ! dI1
      INTER_dI1(:) = dI1(SZA_MIN,VZA_MIN,:) * C11 +
     &               dI1(SZA_MIN,VZA_MAX,:) * C12 +
     &               dI1(SZA_MAX,VZA_MIN,:) * C21 +
     &               dI1(SZA_MAX,VZA_MAX,:) * C22
      ! dI2
      INTER_dI2(:) = dI2(SZA_MIN,VZA_MIN,:) * C11 +
     &               dI2(SZA_MIN,VZA_MAX,:) * C12 +
     &               dI2(SZA_MAX,VZA_MIN,:) * C21 +
     &               dI2(SZA_MAX,VZA_MAX,:) * C22
      ! dIr
      INTER_dIr(:) = dIr(SZA_MIN,VZA_MIN,:) * C11 +
     &               dIr(SZA_MIN,VZA_MAX,:) * C12 +
     &               dIr(SZA_MAX,VZA_MIN,:) * C21 +
     &               dIr(SZA_MAX,VZA_MAX,:) * C22

      ! Return to the calling routine
      END SUBROUTINE INTERPOLATION
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE DETERMINE_SZA_VZA
!
!*****************************************************************************
! Find index for interpolation
!*****************************************************************************
!
      ! Reference to F90 module
      USE LOOKUP_TABLE_MOD,      ONLY : N_SZA,  N_VZA
      USE LOOKUP_TABLE_MOD,      ONLY : SZA,    VZA

      ! Local variables
      INTEGER               :: I

      ! SZA
      DO I = 2, N_SZA, 1
      IF ( OMI_SZA >= SZA(I-1) .AND. 
     &     OMI_SZA <= SZA(I)         ) THEN
         SZA_MIN = I - 1
         SZA_MAX = I
      END IF
      END DO

      ! VZA
      DO I = 2, N_VZA, 1
      IF ( OMI_VZA >= VZA(I-1) .AND.
     &     OMI_VZA <= VZA(I)         ) THEN
         VZA_MIN = I - 1
         VZA_MAX = I
      END IF
      END DO

      END SUBROUTINE DETERMINE_SZA_VZA
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE MAKE_CURRENT_OMI_SO2
!
!*****************************************************************************
!  Subroutine MAKE_CURRENT_OMI_SO2 output some dignostic data for
!  current ND49 time
!  (ywang, 07/24/15)
!
!*****************************************************************************
!
      ! Reference to f90 module
      USE BPCH2_MOD
      USE DIRECTORY_MOD,     ONLY : DIAG_DIR
      USE inquireMod,        ONLY : findFreeLUN
      USE READ_ND49_MOD,     ONLY : NI,     NJ
      USE READ_ND49_MOD,     ONLY : IFIRST, JFIRST
      USE TIME_MOD,          ONLY : EXPAND_DATE
      USE TIME_MOD,          ONLY : GET_NYMD, GET_NHMS
      USE TIME_MOD,          ONLY : GET_TAU

      ! Local Variables
      INTEGER              :: I,     J,   L
      CHARACTER(LEN=255)   :: FILENAME
      INTEGER              :: IU_FILE

      ! For binary punch file, version 2.0
      REAL*4               :: LONRES, LATRES
      INTEGER, PARAMETER   :: HALFPOLAR = 1
      INTEGER, PARAMETER   :: CENTER180 = 1

      CHARACTER(LEN=255)   :: OUTPUT_FILE
      CHARACTER(LEN=20)    :: MODELNAME
      CHARACTER(LEN=40)    :: CATEGORY
      CHARACTER(LEN=40)    :: UNIT
      CHARACTER(LEN=40)    :: RESERVED = ''
      CHARACTER(LEN=80)    :: TITLE

      !=================================================================
      ! MAKE_CURRENT_OMI_SO2 begins here!
      !=================================================================

      ! Find a free file LUN
      IU_FILE = findFreeLUN()

      ! Hardwire output file for now
      OUTPUT_FILE = 'gctm.omi.so2.YYYYMMDD.hhmm'

      ! Define variables for BINARY PUNCH FILE OUTPUT
      TITLE    = 'GEOS-CHEM diag File: ' //
     &           'OMI SO2'
      UNIT     = 'DU'
      CATEGORY = 'IJ-AVG-$'
      LONRES   = DISIZE
      LATRES   = DJSIZE

      ! Call GET_MODELNAME to return the proper model name for
      ! the given met data being used
      MODELNAME = GET_MODELNAME()


      ! Copy the output observation file name into a local variable
      FILENAME = TRIM( OUTPUT_FILE )

      ! Replace YYMMDD and hhmmss token w/ actucl value
      CALL EXPAND_DATE( FILENAME, GET_NYMD(), GET_NHMS() )

      FILENAME = TRIM( DIAG_DIR ) // TRIM( FILENAME )

      WRITE( 6, 100 ) TRIM( FILENAME )
 100  FORMAT( '     - MAKE_CURRENT_OMI_SO2:  Writing ', a )

      ! Open file for output
      CALL OPEN_BPCH2_FOR_WRITE( IU_FILE, FILENAME, TITLE )

      CURR_DIAG = 0.0

!$OMP PARALLEL DO
!$OMP+DEFAULT( SHARED )
!$OMP+PRIVATE( I, J )
      DO J = 1, NJ
      DO I = 1, NI

         IF ( CURR_COUNT(I,J) > 0 ) THEN

            CURR_DIAG(I,J,1) = REAL( CURR_GC_V_SO2(I,J),  4 ) /
     &                         REAL( CURR_COUNT(I,J),     4 )
            CURR_DIAG(I,J,2) = REAL( CURR_GC_S_SO2(I,J),  4 ) /
     &                         REAL( CURR_COUNT(I,J),     4 )
            CURR_DIAG(I,J,3) = REAL( CURR_AMF(I,J),       4 ) /
     &                         REAL( CURR_COUNT(I,J),     4 )
            CURR_DIAG(I,J,4) = REAL( CURR_OMI_V_SO2(I,J), 4 ) /
     &                         REAL( CURR_COUNT(I,J),     4 )
            CURR_DIAG(I,J,5) = REAL( CURR_OMI_S_SO2(I,J), 4 ) /
     &                         REAL( CURR_COUNT(I,J),     4 )
            CURR_DIAG(I,J,6) = REAL( CURR_V_SO2(I,J),     4 ) /
     &                         REAL( CURR_COUNT(I,J),     4 )
            CURR_DIAG(I,J,7) = REAL( CURR_COUNT(I,J),     4 )

         END IF

      ENDDO
      ENDDO
!$OMP END PARALLEL DO

      CALL BPCH2( IU_FILE,   MODELNAME, LONRES,    LATRES,
     &            HALFPOLAR, CENTER180, CATEGORY,  26,
     &            UNIT,      GET_TAU(), GET_TAU(), RESERVED,
     &            NI,        NJ,        7,         IFIRST,
     &            JFIRST,    1,         CURR_DIAG )

      ! Close file
      CLOSE( IU_FILE )

      ! Return to the calling routines
      END SUBROUTINE MAKE_CURRENT_OMI_SO2
!
!-----------------------------------------------------------------------------
!
      SUBROUTINE MAKE_ALL_OMI_SO2
!
!*****************************************************************************
!  Subroutine MAKE_ALL_OMI_SO2 output some dignostic data for
!  average 
!  (ywang, 07/24/15)
!
!*****************************************************************************
!
      ! Reference to f90 module
      USE BPCH2_MOD
      USE DIRECTORY_MOD,     ONLY : DIAG_DIR
      USE inquireMod,        ONLY : findFreeLUN
      USE READ_ND49_MOD,     ONLY : NI,     NJ
      USE READ_ND49_MOD,     ONLY : IFIRST, JFIRST
      USE TIME_MOD,          ONLY : EXPAND_DATE
      USE TIME_MOD,          ONLY : GET_NYMDb
      USE TIME_MOD,          ONLY : GET_TAUb

      ! Local Variables
      INTEGER              :: I,     J,   L
      CHARACTER(LEN=255)   :: FILENAME
      INTEGER              :: IU_FILE

      ! For binary punch file, version 2.0
      REAL*4               :: LONRES, LATRES
      INTEGER, PARAMETER   :: HALFPOLAR = 1
      INTEGER, PARAMETER   :: CENTER180 = 1

      CHARACTER(LEN=255)   :: OUTPUT_FILE
      CHARACTER(LEN=20)    :: MODELNAME
      CHARACTER(LEN=40)    :: CATEGORY
      CHARACTER(LEN=40)    :: UNIT
      CHARACTER(LEN=40)    :: RESERVED = ''
      CHARACTER(LEN=80)    :: TITLE

      !=================================================================
      ! MAKE_ALL_OMI_SO2 begins here!
      !=================================================================

      ! Find a free file LUN
      IU_FILE = findFreeLUN()

      ! Hardwire output file for now
      OUTPUT_FILE = 'gctm.omi.so2.ave.YYYYMMDD'

      ! Define variables for BINARY PUNCH FILE OUTPUT
      TITLE    = 'GEOS-CHEM diag File: ' //
     &           'OMI SO2'
      UNIT     = 'DU'
      CATEGORY = 'IJ-AVG-$'
      LONRES   = DISIZE
      LATRES   = DJSIZE

      ! Call GET_MODELNAME to return the proper model name for
      ! the given met data being used
      MODELNAME = GET_MODELNAME()

      ! Copy the output observation file name into a local variable
      FILENAME = TRIM( OUTPUT_FILE )

      ! Replace YYMMDD and hhmmss token w/ actucl value
      CALL EXPAND_DATE( FILENAME, GET_NYMDb(), 9999 )

      FILENAME = TRIM( DIAG_DIR ) // TRIM( FILENAME )

      WRITE( 6, 100 ) TRIM( FILENAME )
 100  FORMAT( '     - MAKE_ALL_OMI_SO2:  Writing ', a )

      ! Open file for output
      CALL OPEN_BPCH2_FOR_WRITE( IU_FILE, FILENAME, TITLE )

      ALL_DIAG = 0.0

!$OMP PARALLEL DO
!$OMP+DEFAULT( SHARED )
!$OMP+PRIVATE( I, J )
      DO J = 1, NJ
      DO I = 1, NI

         IF ( ALL_COUNT(I,J) > 0 ) THEN

            ALL_DIAG(I,J,1) = REAL( ALL_GC_V_SO2(I,J),  4 ) /
     &                        REAL( ALL_COUNT(I,J),     4 )
            ALL_DIAG(I,J,2) = REAL( ALL_GC_S_SO2(I,J),  4 ) /
     &                        REAL( ALL_COUNT(I,J),     4 )
            ALL_DIAG(I,J,3) = REAL( ALL_AMF(I,J),       4 ) /
     &                        REAL( ALL_COUNT(I,J),     4 )
            ALL_DIAG(I,J,4) = REAL( ALL_OMI_V_SO2(I,J), 4 ) /
     &                        REAL( ALL_COUNT(I,J),     4 )
            ALL_DIAG(I,J,5) = REAL( ALL_OMI_S_SO2(I,J), 4 ) /
     &                        REAL( ALL_COUNT(I,J),     4 )
            ALL_DIAG(I,J,6) = REAL( ALL_V_SO2(I,J),     4 ) /
     &                        REAL( ALL_COUNT(I,J),     4 )
            ALL_DIAG(I,J,7) = REAL( ALL_COUNT(I,J),     4 )

         END IF

      ENDDO
      ENDDO
!$OMP END PARALLEL DO

      CALL BPCH2( IU_FILE,   MODELNAME,  LONRES,    LATRES,
     &            HALFPOLAR, CENTER180,  CATEGORY,  26,
     &            UNIT,      GET_TAUb(), GET_TAUb(), RESERVED,
     &            NI,        NJ,         7,         IFIRST,
     &            JFIRST,    1,          ALL_DIAG )

      ! Close file
      CLOSE( IU_FILE )

      ! Return to the calling routines
      END SUBROUTINE MAKE_ALL_OMI_SO2
!
!-----------------------------------------------------------------------------
!
      ! End of module
      END MODULE AMF_MOD
